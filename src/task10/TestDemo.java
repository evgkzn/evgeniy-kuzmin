package task10;

import java.util.Scanner;

public class TestDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[][] questions = new String[5][4];
        String[] question1 = new String[]{"What year is now?", "2019", "2017", "*2018"};
        String[] question2 = new String[]{"What university we are studying in?", "*KFU", "MGU", "MFTI"};
        String[] question3 = new String[]{"2+2*2=?", "8", "*6", "4"};
        String[] question4 = new String[]{"sin(pi/2)=?", "*1", "0", "-1"};
        String[] question5 = new String[]{"What weighs more: a kilogram of feathers or a kilogram of nails", "Feathers", "Nails", "*Equally"};
        questions[0] = question1;
        questions[1] = question2;
        questions[2] = question3;
        questions[3] = question4;
        questions[4] = question5;
        String input;
        int rightAnswers = 0;
        String rightAnswer = null;
        System.out.println("Hello!Welcome to our test");
        for (int i=0;i<5;i++) {
                for (int j=0;j<4;j++) {
                    if (questions[i][j].charAt(0) != '*') {
                        System.out.println(questions[i][j]);
                    } else {
                        System.out.println(questions[i][j].substring(1,questions[i][j].length()));
                        rightAnswer = questions[i][j].substring(1,questions[i][j].length());
                    }
                }
            input=sc.nextLine();
                if(input.equals(rightAnswer)) {
                    rightAnswers++;
                }
        }
        System.out.println("Test is over. Your result is " + rightAnswers + "/5");
    }
}
