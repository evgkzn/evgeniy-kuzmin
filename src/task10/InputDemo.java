package task10;
import java.util.Scanner;
public class InputDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name;
        int age =0;
        String isOk;

        String input;
        System.out.println("What is your name?");
        input = sc.nextLine();
        name = input;
        if (input !=null) {
            System.out.println("How old are you?");
            input = sc.nextLine();
            while(!((Integer.parseInt(input)<150)&(Integer.parseInt(input)>0))) {
                System.out.println("ERROR: Incorrect age. Please, print it again");
                input = sc.nextLine();
            }
            age = Integer.parseInt(input);
        }
        if (input !=null) {
            System.out.println("Hello, " + name);
            input = sc.nextLine();
        }
        if (input !=null) {
            System.out.println("How are you?");
            input = sc.nextLine();
            if (input.equals("OK")) {
                System.out.println("Good!");
            } else if (input.equals("Bad")) {
                System.out.println("Ohh... Sorry");
            }
        }

        if (input !=null){
            System.out.println("Any questions?");
            input = sc.nextLine();
            while(!input.equals("Quit")) {
                switch (input) {
                    case "What is my name?":
                        System.out.println("Your name is " + name);
                        System.out.println("Anything else?");
                        input = sc.nextLine();
                        break;
                    case "How old am I?":
                        System.out.println("You are " + age + " years old");
                        System.out.println("Anything else?");
                        input = sc.nextLine();
                        break;
                    default:
                        System.out.println("Sorry, I don't know anything about it");
                        input = sc.nextLine();
                        break;
                }
            }
            System.out.println("Bye!");
        }

    }
}
