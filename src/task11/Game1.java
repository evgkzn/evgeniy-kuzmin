package task11;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Scanner;
public class Game1 {
    public static void printArray(Character[][] array){
        System.out.println("  a b c d e f ");
        for (int i=0;i<6;i++){
            System.out.print(i+1+" ");
            for(int j=0;j<6;j++) {
                if(!array[i][j].equals(' ')) {
                    System.out.print("* ");
                } else System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void printChosenCouple(String input, Character[][] array) {
        String cell1 = input.substring(0, input.indexOf(' '));
        String cell2 = input.substring(input.indexOf(' ') + 1, input.length());
        System.out.println(cell1);
        System.out.println(cell2);
        int cell1J = getICell(cell1);
        int cell1I = (Integer.parseInt(cell1.substring(1)) - 1);
        int cell2J = getICell(cell2);
        int cell2I = (Integer.parseInt(cell2.substring(1)) - 1);
        if ((cell1J == -1) || (cell1I > 5) || (cell1I < 0) || (cell2J == -1) || (cell2I > 5) || (cell2I < 0)) {
            System.out.println();
            System.out.println("Wrong cell ID! Print it again");
            System.out.println();
        } else {
            System.out.println("  a b c d e f ");
            for (int i = 0; i < 6; i++) {
                System.out.print(i + 1 + " ");
                for (int j = 0; j < 6; j++) {
                    if ((i == cell1I) & (j == (cell1J))) {
                        System.out.print(array[cell1I][cell1J] + " ");
                    } else if (((i == cell2I) & (j == (cell2J)))) {
                        System.out.print(array[cell2I][cell2J] + " ");
                    } else System.out.print("* ");
                }
                System.out.println();
            }
            if (array[cell1I][cell1J].equals(array[cell2I][cell2J])) {
                System.out.println();
                System.out.println("You are right!");
                array[cell1I][cell1J] = ' ';
                array[cell2I][cell2J] = ' ';
                System.out.println();
            } else {
                System.out.println();
                System.out.println("Better luck next time");
                System.out.println();
            }
        }
    }
        public static int getICell(String cell){
            switch (cell.charAt(0)){
                case 'a':
                    return 0;
                case 'b':
                    return 1;
                case 'c':
                    return 2;
                case 'd':
                    return 3;
                case 'e':
                    return 4;
                case 'f':
                    return 5;
                    default: return -1;
            }
        }

    public static void randomFilling(Character[][] array) { // { # , @ , & , % }
        int remainingCouples = 18;
        int randomInt;
        for (int l=0;l<18;l++){
            randomInt=ThreadLocalRandom.current().nextInt(1,5);
            switch (randomInt) {
                case 1: // #
                    fillCouple('#',array);
                    break;
                case 2: // @
                    fillCouple('@',array);
                    break;
                case 3: // &
                    fillCouple('&',array);
                    break;
                case 4: // %
                    fillCouple('%',array);
                    break;
            }
        }
    }


    public static boolean isPositionFree(int i, int j, Character[][] array) {
        if (array[i][j]==null) {
            return true;
        } else return false;
    }

    public static void fillCouple(Character c, Character[][] array) {
        int randomI;
        int randomJ;
        boolean isOk;
        for (int k = 0; k < 2; k++) {
            do {
                randomI = ThreadLocalRandom.current().nextInt(0, 6);
                randomJ = ThreadLocalRandom.current().nextInt(0, 6);
                isOk = isPositionFree(randomI,randomJ,array);
            } while(!isOk);
            array[randomI][randomJ] = c;
        }
    }

    public static boolean isGameGoing(Character[][] array){
        for (int i=0;i<6;i++){
            for(int j=0;j<6;j++) {
                if (!array[i][j].equals(' ')) {
                    return true;
                }
            }
        }
        return false;
    }


    public static void main(String[] args) {
        String input;
        Scanner sc = new Scanner(System.in);
        Character[][] array = new Character[6][6];
        randomFilling(array);
        System.out.println("Welcome to our game! Now you need to print two cells you want to compare");
        System.out.println("Print chars a-z first, then 1-25 (Example: a1,y6). Please,print them with a space between (TIP: Print 'Quit' to cancel the game)");
        System.out.println();
        while (isGameGoing(array)==true) {
            printArray(array);
            System.out.println();
                input = sc.nextLine();
                if (((input.charAt(2)!=' ')||((input.length()!=5)))&&(!input.equals("Quit"))){
                    System.out.println();
                System.out.println("Incorrect input. Print your cells again");
                System.out.println();
            }else if(!input.equals("Quit")) {
                    printChosenCouple(input, array);
                }else break;
        }
        System.out.println("Game over!");
    }
}
