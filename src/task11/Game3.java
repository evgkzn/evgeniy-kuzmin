package task11;

import java.util.concurrent.ThreadLocalRandom;
import java.util.Scanner;

public class Game3 {
    public static String getRandomChose() {
        int chose;
        chose = ThreadLocalRandom.current().nextInt(1,4);
        switch (chose){
            case 1: return "Rock";
            case 2: return "Paper";
            case 3: return "Scissors";
            default: return null;
        }
    }

    public static void game(String s) {
        String botChose = getRandomChose();
        if (s.equalsIgnoreCase("Rock")) {
            if (botChose.equalsIgnoreCase("Rock")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("Draw!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Paper")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You lose!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Scissors")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You win!");
                botChose = getRandomChose();
            }
        } else if (s.equalsIgnoreCase("Paper")) {
            if (botChose.equalsIgnoreCase("Rock")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You win!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Paper")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("Draw!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Scissors")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You lose!");
                botChose = getRandomChose();
            }
        } else if (s.equalsIgnoreCase("Scissors")) {
            if (botChose.equalsIgnoreCase("Rock")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You lose!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Paper")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("You win!");
                botChose = getRandomChose();
            } else if (botChose.equalsIgnoreCase("Scissors")) {
                System.out.println("Bot chose: " + botChose);
                System.out.println("Draw!");
                botChose = getRandomChose();
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = null;
        System.out.println("Game started! Print your chose: Rock/Paper/Scissors (Or Quit if you want to quit) : ");
        input = sc.nextLine();
        while (!input.equalsIgnoreCase("Quit")) {
            game(input);
            System.out.println("Print your chose: rock/paper/scissors : ");
            input = sc.nextLine();
        }
    }

}
