package task11;

public class ArrayMathDemo {

    public static int findSum(Integer[] a) {
        int sum= 0;
        for (int i=0;i<a.length;i++){
            sum = sum+a[i];
        }
        return sum;
    }

    public static int findMax(Integer[] a) {
        int maxInt = Integer.MIN_VALUE;
        for (int i=0;i<a.length;i++){
            if (a[i]>maxInt){
                maxInt = a[i];
            }
        }
        return maxInt;
    }

    public static int findMin(Integer[] a) {
        int minInt = Integer.MAX_VALUE;
        for (int i=0;i<a.length;i++){
            if (a[i]<minInt){
                minInt = a[i];
            }
        }
        return minInt;
    }

    public static double findAvg(Integer[] a){
        double avg = 0;
        avg = (findSum(a))/(a.length);
        return avg;
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{5,7,8,10,15,2,3};
        System.out.println("Sum: " + findSum(a));
        System.out.println("Min: " + findMin(a));
        System.out.println("Max: " + findMax(a));
        System.out.println("Avg: "+ findAvg(a));
    }
}
