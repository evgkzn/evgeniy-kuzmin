package map;
import java.util.Scanner;

/**
 * @version 1.0
 * @author Evgeniy Kuzmin
 */
public class MapDemo {
    public static void main(String[] args) {
        Movement move = new Movement();
        MapCoords mc = new MapCoords();
        MapDisplay md = new MapDisplay();
        MapArea map = new MapArea(10,10);
        map.placeObject("*", 10,5);
        map.placeObject("-", 1,1);
        md.display(map.getMap());
        move.move(map.getMap());

    }
}
