package map;

public class MapArea {
    private int width; // Area's width
    private int hight; // Area's hight
    private Cell[][] mapArray = null; // Array of cells

    // Returns width
    public int getWidth() {
        return width;
    }

    // Returns hight
    public int getHight() {
        return hight;
    }

    // Returns array of cells
    public Cell[][] getMap() {
        return mapArray;
    }

    // Creates array of cells
    public MapArea(int hight, int width) {
        if ((hight>26)||(width>26)||(hight<1)||(width<1)){
            System.out.println("MapArea Error : Incorrect hight or width");
        }else {
            this.mapArray = new Cell[width][hight];
            this.hight = hight;
            this.width = width;
            for (int i = 0; i < mapArray.length; i++) {
                for (int j = 0; j < mapArray[0].length; j++) {
                    mapArray[i][j] = new Cell();
                }
            }
        }
    }

    // Puts the object on the desired coordinates
    public void placeObject(String object, int y , int x) {
            if (mapArray==null){
            System.out.println("PlaceObject Error : Create map first");
        } else
            if ((y>mapArray.length)||(x>mapArray[0].length)||(y<1)||(x<1)){
                System.out.println("PlaceObject Error : Incorrect input.");
            }else {
                mapArray[x-1][y-1].setObject(object);
                mapArray[x-1][y-1].setDisplaySymbol(object);
            }
    }
}
