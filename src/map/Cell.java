package map;

public class Cell implements MapPlaceable{
    private String DisplaySymbol = "▢"; // Displayed symbol
    private String object = "▢"; // Symbol in cell
    public String emptyCellObject = "▢"; // Symbol of empty cell

    // Fills a cell with an object
    public void setObject(String object) {
        this.object = object;
    }

    // Returns object from cell
    public String getObject(){ // Возвращает сам обьект
        return this.object;
    }

    // Sets displayed symbol
    public void setDisplaySymbol(String displaySymbol) { // Устанавливает новый символ
        DisplaySymbol = displaySymbol;
    }

    // Returns displayed symbol
    public String getDisplaySymbol() {
        return DisplaySymbol;
    }

    // Checks cell: empty or not
    public boolean isEmpty(int x, int y) {
        if (this.object.equals("▢")){
            return true;
        } else {
            return false;
        }
    }
}
