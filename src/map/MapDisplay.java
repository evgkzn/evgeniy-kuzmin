package map;

public class MapDisplay {

    // Displays array of cells as a map
    public void display(Cell[][] array){
        System.out.print("   ");
        for (int i=0;i<array[0].length;i++){
            System.out.print(Character.toString((char)(97+i))+" ");
        }
        System.out.println();
        for (int j=1;j<array.length+1;j++){
            if (j<10) {
                System.out.print(Integer.toString(j) + "  ");
            } else {
                System.out.print(Integer.toString(j) + " ");
            }
            for (int k=0;k<array[0].length;k++){
                System.out.print(array[j-1][k].getDisplaySymbol() + " ");
            }
            System.out.println();
        }
    }
}
