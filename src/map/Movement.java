package map;
import java.util.Scanner;
public class Movement {

    //Method for moving objects in specific cell array
    public void move(Cell[][] array) {
        String input = null;
        Scanner sc = new Scanner(System.in);
        MapCoords mc = new MapCoords();
        Cell cell = new Cell();
        int coordY = -1;
        int coordX = -1;
        boolean isWorkingWithCell = true;
        String chosenDirection = null;
        while (isWorkingWithCell) {
            System.out.println();
            System.out.println("Print coordinates of the object you want to move: (Example: j5)");
            System.out.println("Or print 'quit' to quit");
            input = sc.nextLine();
            if (!input.equalsIgnoreCase("quit")) {
                input = mc.convert(input);
                coordY = Integer.parseInt(input.substring(0, input.indexOf(' ')));
                coordX = Integer.parseInt(input.substring(input.indexOf(' ') + 1, input.length()));
                coordY--;
                coordX--;
                if (array[coordX][coordY].getObject().equals(cell.emptyCellObject)) {
                    System.out.println("There is nothing in this cell. Select another cell.");
                } else {
                    isWorkingWithCell = true;
                    System.out.println("Print a direction to move: (north/south/west/east)");
                    System.out.println("Or print 'quit' to quit");
                    chosenDirection = sc.nextLine();
                    chosenDirection = chosenDirection.toUpperCase();
                    switch (chosenDirection) {
                        case "NORTH":
                            moveOperationing(array, array[coordX][coordY], coordY, coordX, Direction.NORTH);
                            break;
                        case "SOUTH":
                            moveOperationing(array, array[coordX][coordY], coordY, coordX, Direction.SOUTH);
                            break;
                        case "WEST":
                            moveOperationing(array, array[coordX][coordY], coordY, coordX, Direction.WEST);
                            break;
                        case "EAST":
                            moveOperationing(array, array[coordX][coordY], coordY, coordX, Direction.EAST);
                            break;
                        default:
                            System.out.println("Incorrect input. Print it again.");
                    }
                }
            } else {
                isWorkingWithCell = false;
            }

        }
    }

    //Method for working with specific cell in array of cells with it's coordinates and directory of moving
    private void moveOperationing(Cell[][] array, Cell cell,int y, int x, Direction dir) {
        MapDisplay md = new MapDisplay();
        int permY = -1;
        int permX = -1;
        if ((y-dir.getY()<0)||(y-dir.getY()>array[0].length-1)||(x-dir.getX()<0)||(x-dir.getX()>array.length-1)){
            System.out.println("No space");
        } else {
            permY = y;
            permX = x;
            if ((array[x-dir.getX()][y-dir.getY()].getObject()).equals(cell.emptyCellObject)){
                array[x-dir.getX()][y-dir.getY()].setObject(array[permX][permY].getObject());
                array[x-dir.getX()][y-dir.getY()].setDisplaySymbol(array[permX][permY].getDisplaySymbol());
                array[permX][permY].setObject(cell.emptyCellObject);
                array[permX][permY].setDisplaySymbol(cell.emptyCellObject);
                System.out.println();
                md.display(array);
            }else {
                array[x-dir.getX()][y-dir.getY()].setObject(array[permX][permY].getObject());
                array[permX][permY].setObject(cell.emptyCellObject);
                array[permX][permY].setDisplaySymbol(cell.emptyCellObject);
                System.out.println();
                md.display(array);
            }
        }
    }
}
