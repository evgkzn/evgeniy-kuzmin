package map;

public interface MapPlaceable {
    String DisplaySymbol = null;

    // Returns displayed symbol
    public String getDisplaySymbol();
}
