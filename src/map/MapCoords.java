package map;

public class MapCoords {

    // Converts coordinates: example: from "a1" to "1 1"
    public String convert(String input) {
        String output = "";
        if ((input.length()<2)||(input.length()>3)){
            System.out.println("Convert Error : Wrong converting input.");
            return null;
        } else {
            int firstLetter = -1;
            String secondLetter = "";
            firstLetter = (int) (input.charAt(0));
            if (input.length()==2) {
                for (int k=48;k<72;k++){
                    if ((int)(input.charAt(1))==k) {
                        secondLetter += (k-48);
                    }
                }
            } else if (input.length()==3){
                for (int m=1;m<3;m++){
                    for (int l=48;l<72;l++) {
                        if ((int) (input.charAt(m)) == l) {
                        secondLetter += (l-48);
                        }
                    }
                }
            } else System.out.println("Convert Error : Wrong converting input.");

            for (int i=97;i<123;i++){
                if (i==firstLetter){
                    output = output + (i-96) + " ";
                } else if ((i==122)&&(output.equals(""))){
                    System.out.println("Convert Error : Wrong converting input.");
                    return null;
                }
            }
            int kol = 0;
            for (int j=1;j<27;j++){
                if (j==Integer.parseInt(secondLetter)){
                    output = output + j;
                    kol++;
                } else if ((j==26)&&(kol==0)){
                    System.out.println("Convert Error : Wrong converting input.");
                }
            }
            return output;
        }
    }
}
