package map;

// Contains 4 directions for movement
public enum Direction {
    NORTH(0,1),
    EAST(1,0),
    SOUTH(0,-1),
    WEST(-1,0);
    private final int x;
    private final int y;

    // Constructor for Direction
    private Direction(int y, int x) {
        this.x = x;
        this.y = y;
    }

    // Returns X coordinate
    public int getX() { // Returns X
        return x;
    }

    // Returns Y coordinate
    public int getY() { // Returns Y
        return y;
    }

}
