package task13;

public interface Placeable {
    public void setCoords(int x, int y);
    public int[] getCoords();
}
