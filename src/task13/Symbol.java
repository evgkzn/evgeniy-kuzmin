package task13;

public class Symbol implements Printable{
    private Character symbol;

    public Symbol(Character symbol) {
        this.symbol = symbol;
    }

    public void print(){
        System.out.println(symbol);
    }
}
