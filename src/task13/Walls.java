package task13;

public class Walls implements Colorable {

    public String getColor() {
        return color;
    }

    private String color = "";

    public Walls(String color) {
        this.color = color;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }
}
