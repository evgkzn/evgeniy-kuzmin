package task13;
import java.util.Scanner;
public class Demo {
    public static void main(String[] args) {
        //Placeble interface
        Dot d1 = new Dot(2,3);
        Dot d2 = new Dot(4,8);
        System.out.println("d1: " + "x: " + d1.getCoords()[0] + ", y: " + d1.getCoords()[1]);
        System.out.println("d2: " + "x: " + d2.getCoords()[0] + ", y: " + d2.getCoords()[1]);
        System.out.println("Change d2 coords to (7,8) :");
        d2.setCoords(7,8);
        System.out.println("d2: " + "x: " + d2.getCoords()[0] + ", y: " + d2.getCoords()[1]);
        System.out.println();

        //Movable interface
        String[][] array = new String[2][2];
        array[0][0] = "Alive: ";
        array[1][0] = "Not alive: ";
        Objects car = new Objects("Car");
        Objects bird = new Objects("Bird");
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose where to move - " + car.getName());
        System.out.println("If to alive, print 1; If to not alive, print 2");
        // This must work with scanner - int choice = sc.nextInt();
        int choice = 1;
        car.moveTo(array,choice-1);
        System.out.println("Choose where to move - " + bird.getName());
        System.out.println("If to alive, print 1; If to not alive, print 2");
        // This must work with scanner - choice = sc.nextInt();
        choice = 2;
        bird.moveTo(array,choice-1);
        System.out.println("Here is the result: ");
        for (int i=0;i<2;i++) {
            for (int j=0;j<2;j++){
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
        System.out.println();

        //Colorable interface
        Walls w1 = new Walls("Red");
        Walls w2 = new Walls("Blue");
        System.out.println("w1 color: " + w1.getColor() + " w2 color : " + w2.getColor());
        System.out.println();

        //Printable interface
        Symbol dot = new Symbol('.');
        Symbol plus = new Symbol('+');
        System.out.print("Dot symbol: ");
        dot.print();
        System.out.print("Plus symbol: ");
        plus.print();

    }
}
