package task13;

public class Dot implements Placeable{
    private int[] coords = new int[2];

    public Dot(int x, int y) {
        this.coords[0]= x;
        this.coords[1] = y;
    }

    @Override
    public void setCoords(int x, int y) {
        this.coords[0] = x;
        this.coords[1] = y;
    }

    public int[] getCoords(){
        return coords;
    }
}
