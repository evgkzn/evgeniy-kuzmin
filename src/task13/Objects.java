package task13;

public class Objects implements Movable{
    public String getName() {
        return name;
    }

    private String name = "";

    public Objects(String name) {
        this.name = name;
    }

    public void moveTo(String[][] array, int list){
        array[list][1] = this.name;
    }
}
