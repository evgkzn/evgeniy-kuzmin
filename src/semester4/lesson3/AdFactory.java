package semester4.lesson3;

public interface AdFactory {
    Ad createAd(String url);
}
