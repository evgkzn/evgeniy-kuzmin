package semester4.lesson3;

public interface Interpreter {
    public void handle(String line);
}
