package semester4.lesson3;

import java.util.Map;

public class PageHelper {

    static Map<String, Page> pages;

    public static void create() {
        PageReader pageReader = new PageReader("pages.txt");
        pages = pageReader.readPages();
    }

    public static Page getPage(String url) {
        Page page;
        if ((page = pages.get(url)) != null) {
            return page;
        } else {
            System.out.println("Error 404");
            return null;
        }
    }
}