package semester4.lesson3;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VideoAd implements Ad{
    private boolean isWatched;
    private String adUrl;
}
