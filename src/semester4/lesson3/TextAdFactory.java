package semester4.lesson3;

public class TextAdFactory implements AdFactory {
    private static TextAdFactory textAdFactory;

    public static TextAdFactory getInstance() {
        if (textAdFactory == null) {
            textAdFactory = new TextAdFactory();
        }
        return textAdFactory;
    }

    @Override
    public Ad createAd(String url) {
        return new TextAd(url);
    }
}