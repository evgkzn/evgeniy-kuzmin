package semester4.lesson3;

public class BrowserHelper {
    private static Browser browser;

    public static Browser getBrowser() {
        return browser;
    }

    public static void setBrowser(Browser browser) {
        BrowserHelper.browser = browser;
    }
}