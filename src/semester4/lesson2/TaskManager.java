package semester4.lesson2;

import java.util.HashMap;

public class TaskManager {
    private HashMap<Integer, TaskInterface> tasks = new HashMap<>();

    public Object perform(String command){

        if (command.startsWith("new task")){
            return addTask(command);
        }

        if (command.startsWith("up")){
            return upTask(divideCommand(command));
        }

        if (command.startsWith("down")){
            return downTask(divideCommand(command));
        }

        if (command.startsWith("copy")){
            int result = copyTask(divideCommand(command));
            if (result!=0){
                return result;
            } else return null;
        }

        printError();
        return null;
    }

    private int addTask(String command){
        command = command.replace("new task ", "");
        TaskInterface newTask = new Task(command);
        tasks.put(newTask.getId(), newTask);
        return newTask.getId();
    }

    private boolean upTask(String[] commands){
        try{
            int taskID = Integer.parseInt(commands[1]);
            String[] args = new String[commands.length-2];
            for (int i = 2; i < commands.length; i++) {
                args[i-2] = commands[i];
            }
            TaskProxy taskProxy = new TaskProxy(tasks.get(taskID));
            taskProxy.up(args);
            return true;
        }
        catch (Exception e){
            printError();
            return false;
        }
    }

    private boolean downTask(String[] commands){
        try{
            int taskID = Integer.parseInt(commands[1]);
            String[] args = new String[commands.length-2];
            for (int i = 2; i < commands.length; i++) {
                args[i-2] = commands[i];
            }
            TaskProxy taskProxy = new TaskProxy(tasks.get(taskID));
            taskProxy.down(args);
            return true;
        }
        catch (Exception e){
            printError();
            return false;
        }
    }

    private int copyTask(String[] commands){
        try {
            int taskID = Integer.parseInt(commands[1]);
            TaskProxy taskProxy = new TaskProxy(tasks.get(taskID));
            TaskInterface copiedTask = taskProxy.copy();
            tasks.put(copiedTask.getId(),copiedTask);
            return copiedTask.getId();
        }
        catch (Exception e){
            printError();
            return 0;
        }
    }

    private void printError(){
        System.out.println("Something went wrong. Check your data and try again!");
    }

    private String[] divideCommand(String command){
        return command.split(" ");
    }

}
