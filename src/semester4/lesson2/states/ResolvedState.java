package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class ResolvedState extends State {

    @Override
    public void up(Object... args) {
        if(args.length==1){
            getContext().setTesterID(Integer.parseInt((String) args[0]));
            getContext().setState(new InTestingState(getContext()));
        } else {
            System.out.println("Error: Issue with \"Tester ID\" assigning");
        }
    }

    @Override
    public void down(Object... args) {
        getContext().setState(new InProgressState(getContext()));
    }

    public ResolvedState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new ResolvedState(task);
    }
}
