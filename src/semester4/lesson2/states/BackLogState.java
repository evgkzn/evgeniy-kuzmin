package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class BackLogState extends State {

    @Override
    public void up(Object... args) {
        getContext().setState(new OpenState(getContext()));
    }

    @Override
    public void down(Object... args) {

    }

    public BackLogState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new BackLogState(task);
    }
}
