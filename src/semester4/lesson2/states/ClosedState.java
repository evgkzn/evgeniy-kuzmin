package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class ClosedState extends State {

    @Override
    public void up(Object... args) {

    }

    @Override
    public void down(Object... args) {

    }

    public ClosedState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new ClosedState(task);
    }
}
