package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class InTestingState extends State {

    @Override
    public void up(Object... args) {
        getContext().setState(new ClosedState(getContext()));
    }

    @Override
    public void down(Object... args) {
        if(args.length!=0){
            String fullErorString = "";
            for (int i = 0; i < args.length; i++){
                if (i==0){
                    fullErorString = fullErorString + (String) args[i];
                } else {
                    fullErorString = fullErorString + " " + (String) args[i];
                }
            }
            getContext().setTesterID(0);
            getContext().setError(fullErorString);
            getContext().setState(new AssignState(getContext()));

        } else {
            System.out.println("Error: Issue with \"Error\" assigning");
        }
    }

    public InTestingState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new InTestingState(task);
    }
}
