package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class AssignState extends State {

    @Override
    public void up(Object... args) {
        getContext().setState(new InProgressState(getContext()));
    }

    @Override
    public void down(Object... args) {
        getContext().setDeveloperID(0);
        getContext().setState(new OpenState(getContext()));

    }

    public AssignState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new AssignState(task);
    }
}
