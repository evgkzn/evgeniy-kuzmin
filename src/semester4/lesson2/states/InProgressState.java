package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class InProgressState extends State {

    @Override
    public void up(Object... args) {
        getContext().setState(new ResolvedState(getContext()));
    }

    @Override
    public void down(Object... args) {
        getContext().setState(new AssignState(getContext()));
    }

    public InProgressState(Task task) {
        super(task);
    }


    @Override
    public State copy(Task task) {
        return new InProgressState(task);
    }
}
