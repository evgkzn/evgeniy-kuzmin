package semester4.lesson2.states;

import semester4.lesson2.State;
import semester4.lesson2.Task;

public class OpenState extends State {

    @Override
    public void up(Object... args) {
        if(args.length==1){
            getContext().setDeveloperID(Integer.parseInt((String) args[0]));
            getContext().setState(new AssignState(getContext()));
        } else {
            System.out.println("Error: Issue with \"Developer ID\" assigning");
        }
    }

    @Override
    public void down(Object... args) {
        getContext().setState(new BackLogState(getContext()));
    }

    public OpenState(Task task) {
        super(task);
    }

    @Override
    public State copy(Task task) {
        return new OpenState(task);
    }
}
