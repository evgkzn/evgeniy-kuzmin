package semester4.lesson2;

public interface TaskInterface {
    void up(Object[] args);
    void down(Object[] args);
    State getState();
    void setState(State state);
    Task copy();
    int getId();
    int getDeveloperID();
    void setDeveloperID(int developerID);
    int getTesterID();
    void setTesterID(int testerID);
    String getText();
    void setText(String text);
    String getError();
    void setError(String error);
}
