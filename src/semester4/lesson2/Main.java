package semester4.lesson2;

public class Main {
    public static void main(String[] args) {
        TaskManager taskManager = new TaskManager();
        int myNewTaskID = (int) taskManager.perform("new task Write \"Hello World\" application");
        taskManager.perform("up " + myNewTaskID);
        int myNewTask2ID = (int) taskManager.perform("copy 1");
        taskManager.perform("up " + myNewTask2ID + " 376");
        taskManager.perform("down " + myNewTask2ID);
        taskManager.perform("up " + myNewTaskID + " 389");
        taskManager.perform("up " + myNewTaskID);
        taskManager.perform("up " + myNewTaskID);
        taskManager.perform("up " + myNewTaskID + " 983");
        taskManager.perform("up " + myNewTaskID);
        taskManager.perform("up " + myNewTaskID);

    }
}
