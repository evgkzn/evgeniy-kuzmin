package semester4.lesson2;

import java.io.File;
import java.io.FileWriter;

public class TaskProxy implements TaskInterface{

    private TaskInterface task;
    private File file;
    private FileWriter fileWriter;

    public TaskProxy(TaskInterface task) {
        this.task = task;
        file = new File("Lesson2TaskManagerLogs.txt");
        try {
            fileWriter = new FileWriter(file, true);
        } catch (Exception e){ throw new IllegalStateException("Something went wrong.");
        }
    }


    @Override
    public void up(Object[] args) {
        try {
            fileWriter.write("User used \"up\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.up(args);
        } catch (Exception e){}
    }

    @Override
    public void down(Object[] args) {
        try {
            fileWriter.write("User used \"down\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.down(args);
        } catch (Exception e){}
    }

    @Override
    public State getState() {
        try {
            fileWriter.write("User used \"getState\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getState();
        } catch (Exception e){ return task.getState(); }
    }

    @Override
    public void setState(State state) {
        try {
            fileWriter.write("User used \"setState\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.setState(state);
        } catch (Exception e){ task.setState(state); }
    }

    @Override
    public Task copy() {
        try {
            fileWriter.write("User used \"copy\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.copy();
        } catch (Exception e){ return task.copy(); }
    }

    @Override
    public int getId() {
        try {
            fileWriter.write("User used \"getId\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getId();
        } catch (Exception e){ return task.getId(); }
    }

    @Override
    public int getDeveloperID() {
        try {
            fileWriter.write("User used \"getDeveloperID\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getDeveloperID();
        } catch (Exception e){ return task.getDeveloperID(); }
    }

    @Override
    public void setDeveloperID(int developerID) {
        try {
            fileWriter.write("User used \"setDeveloperID\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.setDeveloperID(developerID);
        } catch (Exception e){ task.setDeveloperID(developerID); }
    }

    @Override
    public int getTesterID() {
        try {
            fileWriter.write("User used \"getTesterID\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getTesterID();
        } catch (Exception e){ return task.getTesterID(); }
    }

    @Override
    public void setTesterID(int testerID) {
        try {
            fileWriter.write("User used \"setTesterID\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.setTesterID(testerID);
        } catch (Exception e){ task.setTesterID(testerID); }
    }

    @Override
    public String getText() {
        try {
            fileWriter.write("User used \"getText\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getText();
        } catch (Exception e){ return task.getText(); }
    }

    @Override
    public void setText(String text) {
        try {
            fileWriter.write("User used \"setText\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.setText(text);
        } catch (Exception e){ task.setText(text); }
    }

    @Override
    public String getError() {
        try {
            fileWriter.write("User used \"getError\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            return task.getError();
        } catch (Exception e){ return task.getError(); }
    }

    @Override
    public void setError(String error) {
        try {
            fileWriter.write("User used \"setError\" operation on task #" + task.getId() + "\n");
            fileWriter.close();
            task.setError(error);
        } catch (Exception e){ task.setError(error); }
    }
}
