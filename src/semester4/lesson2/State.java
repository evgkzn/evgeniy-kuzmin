package semester4.lesson2;

public abstract class State {

    private Task task;

    public abstract void up(Object... args);
    public abstract void down(Object... args);

    public State(Task task) {
        this.task = task;
    }

    protected Task getContext(){
        return task;
    }

    public abstract State copy(Task task);
}
