package semester4.lesson2;

import semester4.lesson2.states.DraftState;

public class Task implements TaskInterface{

    public int getId() {
        return id;
    }

    private int id;

    public int getDeveloperID() {
        return developerID;
    }

    public void setDeveloperID(int developerID) {
        this.developerID = developerID;
    }

    public int getTesterID() {
        return testerID;
    }

    public void setTesterID(int testerID) {
        this.testerID = testerID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private int developerID;
    private int testerID;
    private String text;
    private String error;

    private State state;

    public Task(String text) {
        IDHelper idHelper = new IDHelper();
        this.id = idHelper.getID();
        this.state = new DraftState(this);
        this.text = text;
    }

    public void up(Object[] args){
        state.up(args);
    }

    public void down(Object[] args){
        state.down(args);
    }
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Task copy(){
        Task copiedTask = new Task(this.developerID, this.testerID,this.text,this.error, this.state);
        return copiedTask;
    }

    private Task(int developerID, int testerID, String text, String error, State state) {
        IDHelper idHelper = new IDHelper();
        this.id = idHelper.getID();
        this.developerID = developerID;
        this.testerID = testerID;
        this.text = text;
        this.error = error;
        this.state = state.copy(this);
    }
}
