package semester4.lesson2;

public class IDHelper {
    private static int currentID = 0;

    public int getID(){
        return ++currentID;
    }
}
