package semester4.lesson1.iterators;

import semester4.lesson1.Node;
import semester4.lesson1.NodeTree;

import java.util.Comparator;
import java.util.LinkedList;

public class IteratorBFSPriority {

    private Node root;
    private Node currentElement;
    private LinkedList<Node> nodesQueue = new LinkedList<>();

    public IteratorBFSPriority(NodeTree tree) {
        this.root = tree.getRoot();
        this.currentElement = root;
        nodesQueue.add(root);
    }

    public void next(){
        LinkedList<Node> sortingList = new LinkedList<>();
        sortingList.addAll(currentElement.getChildren());
        sortingList.sort(Comparator.comparing(Node::getPriority));
        nodesQueue.addAll(sortingList);
        nodesQueue.remove();
        currentElement = nodesQueue.peek();
    }

    public boolean hasNext(){
        return !nodesQueue.isEmpty();
    }

    public Node getValue(){
        return currentElement;
    }

}
