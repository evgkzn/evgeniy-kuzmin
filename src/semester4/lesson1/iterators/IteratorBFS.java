package semester4.lesson1.iterators;

import semester4.lesson1.Node;
import semester4.lesson1.NodeTree;

import java.util.LinkedList;

public class IteratorBFS {

    private Node root;
    private Node currentElement;
    private LinkedList<Node> nodesQueue = new LinkedList<>();

    public IteratorBFS(NodeTree tree) {
        this.root = tree.getRoot();
        this.currentElement = root;
        nodesQueue.add(root);
        nodesQueue.addAll(root.getChildren());
    }

    public void next(){
        nodesQueue.remove();
        if (!nodesQueue.isEmpty()) {
            currentElement = nodesQueue.peek();
            nodesQueue.addAll(currentElement.getChildren());
        }
    }

    public boolean hasNext(){
        return !nodesQueue.isEmpty();
    }

    public Node getValue(){
        return currentElement;
    }

}
