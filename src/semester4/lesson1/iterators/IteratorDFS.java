package semester4.lesson1.iterators;

import semester4.lesson1.Node;
import semester4.lesson1.NodeTree;

import java.util.LinkedList;

public class IteratorDFS {

    private Node root;
    private Node currentElement;
    private LinkedList<Node> nodesDequeue = new LinkedList<>();

    public IteratorDFS(NodeTree tree) {

        this.root = tree.getRoot();
        createDeque(root);
        this.currentElement = nodesDequeue.peek();
    }

    public void next(){
        nodesDequeue.poll();
        this.currentElement = nodesDequeue.peek();
    }

    public boolean hasNext(){
        return !nodesDequeue.isEmpty();
    }

    public Node getValue(){
        return currentElement;
    }

    private void createDeque(Node node){
        nodesDequeue.add(node);
        if (!(node.getChildren().size() == 0)) {
            for (Node childNode : node.getChildren()) {
                createDeque(childNode);
            }
        }
    }
}
