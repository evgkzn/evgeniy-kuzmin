package semester4.lesson1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(value = {"parent"})
public class Node {
    @XmlElement(name = "name")
     private String name;
    @XmlElement(name = "type")
     private Types type;
    @XmlElement(name = "priority")
     private int priority;
     @XmlTransient
    private Node parent;
    @XmlElement(name = "children")
    private ArrayList<Node> children = new ArrayList<>();;

    public void addChild(Node child) {
        children.add(child);
    }
    public ArrayList<Node> getChildren() {
        return children;
    }
    public String getName() {
        return name;
    }
    public Types getType() {
        return type;
    }
    public int getPriority() {
        return priority;
    }
    public Node getParent() {
        return parent;
    }
    public void setParent(Node parent) {
        this.parent = parent;
    }

    public static class Builder{
        private Node newNode;

        public Builder(){
            newNode = new Node();
        }

        public Builder withName(String name){
            newNode.name = name;
            return this;
        }

        public Builder withType(Types type){
            newNode.type = type;
            return this;
        }

        public Builder withPriority(Integer priority){
            newNode.priority = priority;
            return this;
        }

        public Builder withParent(Node parent){
            newNode.parent = parent;
            return this;
        }

        public Node build(){
            return newNode;
        }
    }
}
