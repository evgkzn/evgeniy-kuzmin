package semester4.lesson1;

import semester4.lesson1.iterators.IteratorBFS;

import java.rmi.UnexpectedException;
import java.util.ArrayList;

public class Interpreter {

    private NodeTree tree;

    public Interpreter(NodeTree tree) {
        this.tree = tree;
    }

    public Object perform(String actionDescription){

        if (actionDescription.startsWith("return children")){
            return returnChildren(actionDescription);
        }

        if (actionDescription.startsWith("add")){
            return addChild(actionDescription);
        }

        if (actionDescription.startsWith("delete")){
            return deleteNode(actionDescription);
        }

        if (actionDescription.startsWith("save")){
            return saveTree(actionDescription);
        }

        printEror();
        return null;
    }

    private Types encodeType(String type){
        switch (type) {
            case "country":
                return Types.COUNTRY;

            case "region":
                return Types.REGION;

            case "district":
                return Types.DISTRICT;

            case "city":
                return Types.CITY;

            case "street":
                return Types.STREET;

            case "house":
                return Types.HOUSE;

            default:
                return null;
        }
    }

    private ArrayList<Node> returnChildren(String actionDescription) {
        String[] commands = actionDescription.split(" ");
        if(commands.length == 4) {
            Types type = encodeType(commands[2]);
            IteratorBFS iteratorBFS = new IteratorBFS(tree);
            while (iteratorBFS.hasNext()){
                if ((iteratorBFS.getValue().getName().equals(commands[3]))&&(iteratorBFS.getValue().getType() == type)) {
                    return iteratorBFS.getValue().getChildren();
                }
                iteratorBFS.next();
            }
            printEror();
            return null;

        } else {
            printEror();
            return null;
        }
    }

    private boolean addChild(String actionDescription){
        String[] commands = actionDescription.split(" ");
        if (commands.length == 5) {
            try {
                String[] path = commands[1].split(",");
                String name = commands[2];
                Types type = encodeType(commands[3]);
                Integer priority = Integer.parseInt(commands[4]);

                Node foundedNode = findNodeByPath(path);

                if((foundedNode == null)||(type == null)) {
                    throw new UnexpectedException("Cant find such node");
                }

                boolean isFoundCopy = false;
                for (Node child: foundedNode.getChildren()){
                    if (child.getName().equalsIgnoreCase(name)){
                        isFoundCopy = true;
                    }
                }
                if(isFoundCopy){
                    throw new IllegalStateException("Founded copy of the node");
                }

                Node newNode = new Node.Builder()
                        .withName(name)
                        .withParent(foundedNode)
                        .withPriority(priority)
                        .withType(type)
                        .build();

                foundedNode.addChild(newNode);
                return true;
            } catch (Exception e){
                printEror();
                return false;
            }
        } else {
                printEror();
                return false;
        }
    }


    private void printEror(){
        System.out.println("Wrong arguments. Try again!");
    }

    private Node findNodeByPath(String[] path){
        if (path.length > 1) {
            Node foundedNode = null;
            IteratorBFS iteratorBFS = new IteratorBFS(tree);
            while (iteratorBFS.hasNext()) {
                foundedNode = checkNodeLevel(iteratorBFS.getValue(), 0, path);
                if (foundedNode!=null){
                    break;
                }
                iteratorBFS.next();
            }
            return foundedNode;

        } else {
            printEror();
            return null;
        }
    }

    private Node checkNodeLevel(Node node, Integer counter, String[] path){
        if ((node.getName().equals(path[counter]))&&(counter<path.length-1)){
            for (Node childNode: node.getChildren()) {
               Node foundedNode = checkNodeLevel(childNode, counter+1, path);
                if (foundedNode!=null){
                    return foundedNode;
                }
            }
        } else if ((counter == path.length - 1)&&(node.getName().equals(path[counter]))){
            return node;
        }
        return null;
    }

    private boolean deleteNode(String actionDescription){
        String[] commands = actionDescription.split(" ");
        if(commands.length == 2) {
            String[] path = commands[1].split(",");
            Node nodeToDelete = findNodeByPath(path);
            if (nodeToDelete!=null) {
                Node parentNode = nodeToDelete.getParent();
                parentNode.getChildren().remove(nodeToDelete);
                return true;
            } else {
                return false;
            }
        } else {
            printEror();
            return false;
        }
    }

    private boolean saveTree(String actionDescription){
        String[] commands = actionDescription.split(" ");
        if (commands.length == 1) {
            JSONWriteReader jsonWriteReader = JSONWriteReader.getInstance();
            jsonWriteReader.writeTree(tree);
            return true;
        }
        printEror();
        return false;
    }
}
