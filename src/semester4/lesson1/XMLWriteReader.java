package semester4.lesson1;

import java.io.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XMLWriteReader implements NodeTreeGetSaveFacade {

    private static XMLWriteReader singleton;

    @Override
    public NodeTree readTree(){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(NodeTree.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            File file = new File("NodeTree.xml");
            NodeTree tree =  (NodeTree) jaxbUnmarshaller.unmarshal(file);
            addParentsToNodes(tree.getRoot());
            return tree;
        } catch (JAXBException exception){
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public void writeTree(NodeTree tree){
        try {
            File file = new File("NodeTree.xml");
            FileWriter fileWriter = new FileWriter(file);
            JAXBContext jaxbContext = JAXBContext.newInstance(NodeTree.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(tree, sw);
            String xmlString = sw.toString();
            fileWriter.write(xmlString + "\n");
            fileWriter.close();
        } catch (Exception exception){
            exception.printStackTrace();
        }
    }

    private void addParentsToNodes(Node node){
        for (Node child : node.getChildren()){
            child.setParent(node);
            addParentsToNodes(child);
        }
    }

    public static XMLWriteReader getInstance() {
        if (singleton == null) {
            singleton = new XMLWriteReader();
        }
        return singleton;
    }
}
