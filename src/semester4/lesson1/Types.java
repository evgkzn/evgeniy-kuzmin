package semester4.lesson1;

public enum Types {
    COUNTRY,
    REGION,
    DISTRICT,
    CITY,
    STREET,
    HOUSE
}
