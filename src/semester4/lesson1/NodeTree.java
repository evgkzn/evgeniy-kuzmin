package semester4.lesson1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "root")
public class NodeTree {

    public NodeTree(Node root) {
        this.root = root;
    }

    public NodeTree(){}

    public Node getRoot() {
        return root;
    }
    @XmlElement(name = "node")
    private Node root;


}
