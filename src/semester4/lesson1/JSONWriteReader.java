package semester4.lesson1;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class JSONWriteReader implements NodeTreeGetSaveFacade {

    private static JSONWriteReader singleton;

    @Override
    public NodeTree readTree() {
        try{
            ArrayList<Node> nodes = new ArrayList<>();
            ObjectMapper objectMapper = new ObjectMapper();
            File file = new File("NodeTree.json");
        FileReader fileReader = new FileReader(file);
        Scanner sc = new Scanner(fileReader);

        Node treeRoot;
            treeRoot = objectMapper.readValue(sc.nextLine(),Node.class);
            addParentsToNodes(treeRoot);
            NodeTree tree = new NodeTree(treeRoot);
            return tree;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void writeTree(NodeTree tree) {
        try {
            File file = new File("NodeTree.json");
            FileWriter fileWriter = new FileWriter(file);
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonString = objectMapper.writeValueAsString(tree.getRoot());
            fileWriter.write(jsonString + "\n");
            fileWriter.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void addParentsToNodes(Node node){
        for (Node child : node.getChildren()){
            child.setParent(node);
            addParentsToNodes(child);
        }
    }

    public static JSONWriteReader getInstance() {
        if (singleton == null) {
            singleton = new JSONWriteReader();
        }
        return singleton;
    }
}
