package semester4.lesson1;

import semester4.lesson1.iterators.IteratorBFS;
import semester4.lesson1.iterators.IteratorBFSPriority;
import semester4.lesson1.iterators.IteratorDFS;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        // Country Russia
        Node russia = new Node.Builder()
                .withName("Russia")
                .withType(Types.COUNTRY)
                .withPriority(1)
                .withParent(null)
                .build();

        // Region Tatarstan
        Node tatarstan = new Node.Builder()
                .withName("Tatarstan")
                .withType(Types.REGION)
                .withPriority(1)
                .withParent(russia)
                .build();
        russia.addChild(tatarstan);

        // Kazan City
        Node kazan = new Node.Builder()
                .withName("Kazan")
                .withType(Types.CITY)
                .withPriority(3)
                .withParent(tatarstan)
                .build();
        tatarstan.addChild(kazan);

        // Profsoyuznaya street
        Node profsoyuznaya = new Node.Builder()
                .withName("Profsoyuznaya")
                .withType(Types.STREET)
                .withPriority(1)
                .withParent(kazan)
                .build();
        kazan.addChild(profsoyuznaya);

        // Prfsoyuznaya house number
        Node profsoyuznayaHouseNumber = new Node.Builder()
                .withName("32")
                .withType(Types.HOUSE)
                .withPriority(1)
                .withParent(profsoyuznaya)
                .build();
        profsoyuznaya.addChild(profsoyuznayaHouseNumber);

        // Elabuga City
        Node elabuga = new Node.Builder()
                .withName("Elabuga")
                .withType(Types.CITY)
                .withPriority(2)
                .withParent(tatarstan)
                .build();
        tatarstan.addChild(elabuga);

        // Raduzhnaya street
        Node raduzhnaya = new Node.Builder()
                .withName("Raduzhnaya")
                .withType(Types.STREET)
                .withPriority(1)
                .withParent(elabuga)
                .build();
        elabuga.addChild(raduzhnaya);

        // Raduzhnaya house number
        Node raduzhnayaHouseNumber = new Node.Builder()
                .withName("5")
                .withType(Types.HOUSE)
                .withPriority(1)
                .withParent(raduzhnaya)
                .build();
        raduzhnaya.addChild(raduzhnayaHouseNumber);

        // Our node tree
        NodeTree tree = new NodeTree(russia);

//         Testing BFS Iterator
        IteratorBFS iteratorBFS = new IteratorBFS(tree);
        while(iteratorBFS.hasNext()) {
            System.out.print(iteratorBFS.getValue().getName());
            iteratorBFS.next();
            if (iteratorBFS.hasNext()) {
                System.out.print(" -> ");
            }
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // Testing DFS Iterator
        IteratorDFS iteratorDFS = new IteratorDFS(tree);
        while(iteratorDFS.hasNext()) {
            System.out.print(iteratorDFS.getValue().getName());
            iteratorDFS.next();
            if (iteratorDFS.hasNext()) {
                System.out.print(" -> ");
            }
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // Testing BFS Priority Iterator
        IteratorBFSPriority iteratorBFSPriority = new IteratorBFSPriority(tree);
        while(iteratorBFSPriority.hasNext()) {
            System.out.print(iteratorBFSPriority.getValue().getName());
            iteratorBFSPriority.next();
            if (iteratorBFSPriority.hasNext()) {
                System.out.print(" -> ");
            }
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // Testing Interpreter Getting Children
        Interpreter interpreter = new Interpreter(tree);
        ArrayList<Node> arrayList = (ArrayList<Node>) interpreter.perform("return children street Raduzhnaya");
        System.out.println(arrayList);

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // Testing Interpreter Adding
        interpreter.perform("add Russia,Tatarstan Chelny city 1");
        IteratorDFS iteratorDFS1 = new IteratorDFS(tree);
        while(iteratorDFS1.hasNext()) {
            System.out.print(iteratorDFS1.getValue().getName());
            iteratorDFS1.next();
            if (iteratorDFS1.hasNext()) {
                System.out.print(" -> ");
            }
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // Testing Interpreter Removing
        interpreter.perform("delete Russia,Tatarstan,Elabuga");
        IteratorDFS iteratorBFS1 = new IteratorDFS(tree);
        while(iteratorBFS1.hasNext()) {
            System.out.print(iteratorBFS1.getValue().getName());
            iteratorBFS1.next();
            if (iteratorBFS1.hasNext()) {
                System.out.print(" -> ");
            }
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // JSON Writer&Reader Test
        JSONWriteReader jsonWriteReader = JSONWriteReader.getInstance();

        try {
            jsonWriteReader.writeTree(tree);
        } catch (Exception e){
            System.out.println("Exception!");
        }

        try {
            NodeTree newTree = jsonWriteReader.readTree();
            IteratorDFS iteratorBFS2 = new IteratorDFS(newTree);
            while(iteratorBFS2.hasNext()) {
                System.out.print(iteratorBFS2.getValue().getName());
                iteratorBFS2.next();
                if (iteratorBFS2.hasNext()) {
                    System.out.print(" -> ");
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        // Delimiter
        System.out.println(" ");
        System.out.println("-----------------------------------");

        // XML Writer&Reader Test
        XMLWriteReader xmlWriteReader = XMLWriteReader.getInstance();

        try {
            xmlWriteReader.writeTree(tree);
        } catch (Exception e){
            System.out.println("Exception!");
        }

        try {
            NodeTree newTree = xmlWriteReader.readTree();
            IteratorDFS iteratorBFS3 = new IteratorDFS(newTree);
            while(iteratorBFS3.hasNext()) {
                System.out.print(iteratorBFS3.getValue().getName());
                iteratorBFS3.next();
                if (iteratorBFS3.hasNext()) {
                    System.out.print(" -> ");
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }


        interpreter.perform("save");
    }
}
