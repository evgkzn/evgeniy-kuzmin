package semester4.lesson1;

public interface NodeTreeGetSaveFacade {
    NodeTree readTree();
    void writeTree(NodeTree tree);
}
