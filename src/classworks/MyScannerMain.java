package classworks;

import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;

public class MyScannerMain {
    public static void main(String[] args) throws IOException {
        File fl = new File("testestes.txt");
        MyScanner ms = new MyScanner(fl);
        System.out.println(ms.nextInt());
    }
}
