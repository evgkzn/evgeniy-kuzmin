package classworks;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;

public class MyScanner {
    private InputStream is;

    public MyScanner(File file) throws FileNotFoundException {
        this.is = new FileInputStream(file);
    }

    public int nextInt() throws IOException {
        String intString = null;
        while ((is.read()!=-1)){
            if (intString == null){
                intString = Character.toString((char)is.read());
            } else {
                intString = intString + (char) is.read();
            }
        }
        return Integer.parseInt(intString);
    }

    public double nextDouble(){
        return 0;
    }

    public String next(){
        return null;
    }

    public String nextLine(){
        return null;
    }
}
