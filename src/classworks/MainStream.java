package classworks;

import java.io.*;

public class MainStream {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("inputNumbers.txt");
        System.out.println((char)is.read());
        System.out.println((char)is.read());
        System.out.println((char)is.read());
        is.close();
    }
}
