package classworks;

public class QueueG<T> {
    private Node first;
    private Node last;

    public void enqueue(T elem){
        Node oldLast = last;
        last = new Node();
        last.value = elem;
        last.next = null;

        if (isEmpty()){
            first = last;
        } else {
            oldLast.next = last;
        }
    }

    public T dequeue(){
        if (isEmpty()){
            throw new IllegalStateException("Queue is empty");
        }
        Node neededNode = first;
        first = first.next;
        return neededNode.value;
    }

    public boolean isEmpty(){
        return first==null;
    }

    class Node {
        T value;
        Node next;
    }
}
