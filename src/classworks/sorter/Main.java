package classworks.sorter;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arr = {6,3,4,2,3,4,7,9};
        Sorter.mergeSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
