package classworks.superbestawesomeframework;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import static classworks.superbestawesomeframework.SuperBestAwesomeFramework.getMany;
import static classworks.superbestawesomeframework.SuperBestAwesomeFramework.getManyWithCons;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {
//        List<SimpleStudent> list =
//                getMany(SimpleStudent.class, 10);
//        System.out.println(list);
        List<SimpleStudent> list1 =
                getManyWithCons(SimpleStudent.class, 10,"John",40);
        System.out.println(list1);
    }
}
