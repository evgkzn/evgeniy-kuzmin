package classworks.superbestawesomeframework;

public class SimpleStudent {
    private int age;
    private String name;

    public SimpleStudent(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public SimpleStudent(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SimpleStudent{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
