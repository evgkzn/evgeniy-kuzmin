package classworks.superbestawesomeframework;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.ArrayList;

public class SuperBestAwesomeFramework {
    public static <T> List<T> getMany(Class<T> c, int count) {
        List<T> list = new ArrayList<>();
        try {
            for (int i = 0; i < count; i++) {
                list.add(c.newInstance());
            }
        } catch (InstantiationException |
                IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        return list;
    }

    //создаем список из count штук объектов типа T,
    //каждый из которых создан путем вызова
    //конструктора класса с набором аргументов args
    public static <T> List<T> getManyWithCons(Class<T> cl, int count,
                                      Object... args) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        List<T> list = new ArrayList<>();
        Constructor<T>[] constructors = (Constructor<T>[]) cl.getDeclaredConstructors();
        Constructor<T> neededConstructor = null;
        for (Constructor<T> cons : constructors){
                    try {
                    cons.newInstance(args);
                    neededConstructor = cons;
                } catch(Exception e){
                    continue;
            }
        }
        if (neededConstructor!=null){
        for (int i = 0; i<count;i++) {
            list.add(neededConstructor.newInstance(args));
        }
            return list;
        } else {
                throw new IllegalStateException("Wrong argsuments");
            }
    }
}
