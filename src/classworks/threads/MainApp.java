package classworks.threads;

import java.util.Random;

public class MainApp {
    public static int sum = 0;
    public static int sum2 = 0;

    public static void main(String[] args) {
        Random rn = new Random();
        int[] array = new int[1000000];
        for (int i: array) {
            i = rn.nextInt(10);
        }
        CounterThread ct1 = new CounterThread(0,100000,array);
        CounterThread ct2 = new CounterThread(100001,200000,array);
        CounterThread ct3 = new CounterThread(200001,300000,array);
        CounterThread ct4 = new CounterThread(300001,400000,array);
        CounterThread ct5 = new CounterThread(400001,500000,array);
        CounterThread ct6 = new CounterThread(500001,600000,array);
        CounterThread ct7 = new CounterThread(600001,700000,array);
        CounterThread ct8 = new CounterThread(700001,800000,array);
        CounterThread ct9 = new CounterThread(800001,900000,array);
        CounterThread ct10 = new CounterThread(900001,999999,array);

        ct1.run();
        ct2.run();
        ct3.run();
        ct4.run();
        ct5.run();
        ct6.run();
        ct7.run();
        ct8.run();
        ct9.run();
        ct10.run();

        for (int i: array) {
            sum2 += i;
        }

        System.out.println(sum == sum2);
    }
}