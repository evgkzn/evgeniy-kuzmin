package classworks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SimpleMapMain {
    public static void main(String[] args) throws FileNotFoundException {
        Map<String, Integer> m1 = new SimpleMap<>();
        File f = new File("input.txt");
        Scanner sc = new Scanner(f);
        while (sc.hasNext()) {
            String s = sc.next();
            s = s.replace(',',' ');
            s = s.replace('(',' ');
            s = s.replace(')',' ');
            s = s.replace('.',' ');
            s = s.replace(';',' ');
            s = s.replace(':',' ');
            s = s.replace('!',' ');
            s = s.replace('?',' ');
            s = s.replace('"',' ');
            s = s.replace('—',' ');
            s = s.replace('»',' ');
            s = s.replace('«',' ');
            s = s.replace('[',' ');
            s = s.replace(']',' ');
            s = s.replaceAll("\\s+","");
            s = s.toLowerCase();

            if (s.equals("")){}
            else {
                if (m1.get(s) == null) {
                    m1.put(s, 1);
                } else {
                    m1.put(s, m1.get(s) + 1);
                }
            }
        }
        ((SimpleMap<String, Integer>) m1).checkKeys();
    }
}
