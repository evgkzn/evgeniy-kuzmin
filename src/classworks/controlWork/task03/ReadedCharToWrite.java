package classworks.controlWork.task03;

public class ReadedCharToWrite {
    public int neededCharCode;
    public boolean isReady;

    public ReadedCharToWrite(char neededCharCode, boolean isReady){
        this.neededCharCode = neededCharCode;
        this.isReady = isReady;
    }
}
