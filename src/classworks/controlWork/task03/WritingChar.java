package classworks.controlWork.task03;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class WritingChar extends Thread{
    private ReadedCharToWrite readedCharToWrite;
    private FileOutputStream fileOutputStream;

    public WritingChar(ReadedCharToWrite readedCharToWrite, String file) throws FileNotFoundException {
        this.readedCharToWrite = readedCharToWrite;
        fileOutputStream = new FileOutputStream(file);
    }

    @Override
    public void run() {
        try{
            while(true){
                synchronized (readedCharToWrite){
                    while(!readedCharToWrite.isReady){
                        readedCharToWrite.wait();
                    }
                    if (readedCharToWrite.neededCharCode == -1){
                        break;
                    }
                    fileOutputStream.write((char) readedCharToWrite.neededCharCode);
                    readedCharToWrite.isReady = false;
                    readedCharToWrite.notify();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
