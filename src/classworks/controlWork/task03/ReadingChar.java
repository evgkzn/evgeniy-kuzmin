package classworks.controlWork.task03;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ReadingChar extends Thread{
    private ReadedCharToWrite readedCharToWrite;
    private FileInputStream fileInputStream;

    public ReadingChar(ReadedCharToWrite readedCharToWrite, String file) throws FileNotFoundException {
        this.readedCharToWrite = readedCharToWrite;
        fileInputStream = new FileInputStream(file);
    }

    @Override
    public void run() {
        try{
            while (true){
                synchronized (readedCharToWrite){
                    while(readedCharToWrite.isReady){
                        readedCharToWrite.wait();
                    }
                    int a = fileInputStream.read();
                    readedCharToWrite.isReady = true;
                    readedCharToWrite.neededCharCode = a;
                    readedCharToWrite.notify();
                    if (a==-1){
                        return;
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
