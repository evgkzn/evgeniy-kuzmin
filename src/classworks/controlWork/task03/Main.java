package classworks.controlWork.task03;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ReadedCharToWrite readedCharToWrite = new ReadedCharToWrite(' ',false);
        ReadingChar readingChar = new ReadingChar(readedCharToWrite, "text1.txt");
        WritingChar writingChar = new WritingChar(readedCharToWrite, "text2.txt");
        readingChar.start();
        writingChar.start();

    }
}
