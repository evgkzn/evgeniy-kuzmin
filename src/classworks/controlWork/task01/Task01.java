package classworks.controlWork.task01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class Task01 {
    public List<String> predicateTest(Predicate<String> predicate) throws FileNotFoundException{
        File file = new File("words.txt");
        Scanner sc = new Scanner(file);
        Map<String, Integer> neededMap = new HashMap<>();
        List<String> inputList = new ArrayList<>();
        List<String> neededList = new ArrayList<>();
        while (sc.hasNext()){
            inputList.add(sc.next());
        }
        for (String i : inputList){
            if (predicate.test(i)){
                if (!neededMap.containsKey(i)){
                    neededMap.put(i,1);
                } else {
                    neededMap.put(i,neededMap.get(i)+1);
                }
            }
        }
        for (int i = 0;i<neededMap.size();i++){
            Integer maxValue = Collections.max(neededMap.values());
            if (maxValue>0){
                for (String j : neededMap.keySet()){
                    if (neededMap.get(j)==maxValue){
                        neededList.add(j);
                        neededMap.put(j,-1);
                    }
                }
            }
        }
        return neededList;
    }
}
