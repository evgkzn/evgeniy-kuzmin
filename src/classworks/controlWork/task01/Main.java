package classworks.controlWork.task01;

import java.io.FileNotFoundException;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        Task01 t1 = new Task01();
        Predicate<String> predicate = x -> x.charAt(0) == 't';
        try {
            System.out.println(t1.predicateTest(predicate));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
