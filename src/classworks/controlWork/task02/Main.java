package classworks.controlWork.task02;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ByAuthorComparator byAuthorComparator = new ByAuthorComparator();
        StorageOfBooks storageOfBooks = new StorageOfBooks();
        List<Book> neededList = storageOfBooks.topN(byAuthorComparator,5);
        for (Book i : neededList){
            System.out.println(i.toString());
        }
    }
}
