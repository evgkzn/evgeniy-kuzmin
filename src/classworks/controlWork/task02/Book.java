package classworks.controlWork.task02;

public class Book implements Comparable<Book>{
    private String name;
    private String author;
    private int yearOfWriting;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getYearOfWriting() {
        return yearOfWriting;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    private int numberOfPages;

    public Book(String name, String author, int yearOfWriting, int numberOfPages) {
        this.name = name;
        this.author = author;
        this.yearOfWriting = yearOfWriting;
        this.numberOfPages = numberOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", yearOfWriting=" + yearOfWriting +
                ", numberOfPages=" + numberOfPages +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        return this.getName().compareTo(o.getName());
    }
}
