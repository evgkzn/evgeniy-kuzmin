package classworks.controlWork.task02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StorageOfBooks {
    private List<Book> storageList = new ArrayList<>();

    public StorageOfBooks(){
        Book book1 = new Book("Anna Karenina", "Lev Tolstoy", 1877, 1000);
        Book book2 = new Book("Robinson Crusoe", "Daniel Defoe", 1719, 500);
        Book book3 = new Book("Gulliver's Travels", "Jonathan Swift", 1726, 700);
        Book book4 = new Book("Clarissa", "Samuel Richardson", 1748, 800);
        Book book5 = new Book("Vanity fair", "William Thackeray", 1848, 999);
        Book book6 = new Book("Moonstone", "Wilkie Collins", 1868, 667);
        Book book7 = new Book("War and peace", "Lev Tolstoy", 1869, 1320);
        Book book8 = new Book("The sign of four", "Arthur Conan Doyle", 1890, 1500);
        Book book9 = new Book("The Picture Of Dorian Gray", "Oscar Wilde", 1891, 1324);
        Book book10 = new Book("Thirty-nine steps", "John Buchan", 1915, 90);
        this.storageList.add(book1);
        this.storageList.add(book2);
        this.storageList.add(book3);
        this.storageList.add(book4);
        this.storageList.add(book5);
        this.storageList.add(book6);
        this.storageList.add(book7);
        this.storageList.add(book8);
        this.storageList.add(book9);
        this.storageList.add(book10);
    }

    public List<Book> topN(Comparator<Book> comparator, int n){
        List<Book> sortedNList = new ArrayList<>();
        if (n>storageList.size()){
            throw new IllegalStateException("Wrong number of books. Print another one. You have only " + storageList.size() + " books.");
        }
        storageList.sort(comparator);
        for (int i = 0;i<n;i++){
            sortedNList.add(storageList.get(i));
        }
        return sortedNList;
    }
}
