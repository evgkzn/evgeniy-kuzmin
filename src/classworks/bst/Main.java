package classworks.bst;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BST<>();
        bst.insert(17);
        bst.insert(26);
        bst.insert(10);
        bst.insert(30);
        bst.insert(5);
        bst.insert(13);
        bst.insert(3);
        bst.insert(40);
        bst.printAll();
        bst.printAllByLevels();
        bst.remove(5);
        bst.printAllByLevels();
    }
}
