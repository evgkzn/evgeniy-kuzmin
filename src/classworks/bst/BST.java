package classworks.bst;

import jdk.nashorn.api.tree.Tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BST<T extends Comparable<T>> implements BinarySearchTree<T> {
    private class TreeNode {
        T value;
        TreeNode left;
        TreeNode right;

        public TreeNode(T value) {
            this.value = value;
        }
    }

    private boolean contains = false;
    private TreeNode root;

    @Override
    public void insert(T elem) {
        this.root = insert(this.root, elem);
    }

    private TreeNode insert(TreeNode root, T elem) {
        if (root == null) {
            root = new TreeNode(elem);
        } else {
            if (root.value.compareTo(elem) >= 0) {
                root.left = insert(root.left, elem);
            } else {
                root.right = insert(root.right, elem);
            }
        }
        return root;
    }

    public boolean remove(T elem)
    {
        root = remove(root, elem);
        if (root!=null){
            return true;
        }
        return false;
    }

    private TreeNode remove(TreeNode root, T elem)
    {
        if (root == null)  return root;

        if (elem.compareTo(root.value) <0)
            root.left = remove(root.left, elem);
        else if (elem.compareTo(root.value)>0)
            root.right = remove(root.right, elem);

        else
        {
            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;
            root.value = minValue(root.right);
            root.right = remove(root.right, root.value);
        }

        return root;
    }

    private T minValue(TreeNode root)
    {
        T minv = root.value;
        while (root.left != null)
        {
            minv = root.left.value;
            root = root.left;
        }
        return minv;
    }

    @Override
    public boolean contains(T elem) {
        return contains(this.root, elem);
    }

    private boolean contains(TreeNode node, T elem) {
        if (contains == false) {
            if (node != null) {
                if (node.value == elem) {
                    contains = true;
                }
                contains(node.left, elem);
                contains(node.right, elem);
            }
        }
        return contains;
    }

    @Override
    public void printAll() {
        printAll(this.root);
    }

    private void printAll(TreeNode root) {
        if (root != null) {
            printAll(root.left);
            System.out.println(root.value);
            printAll(root.right);
        }
    }

public void printAllByLevels() {
    Queue<TreeNode> nodes= new LinkedList<>();

    List<TreeNode> listOfNodes = new ArrayList<TreeNode>();
    traverseLevels(root, listOfNodes,nodes);
    int count = 0,level=0;

    while (count < listOfNodes.size()){
        int printLen= (int) Math.pow(2, level++);

        for (int i=count; i < printLen -1 && i < listOfNodes.size();++i){
            System.out.print(listOfNodes.get(i).value+" ");
        }
        System.out.println();
        count = printLen-1;
    }
}
    private void traverseLevels(TreeNode root, List<TreeNode> listOfNodes, Queue<TreeNode> nodes) {

        if (root!=null){

            nodes.add(root);
            listOfNodes.add(root);
            while(!nodes.isEmpty()){
                root= nodes.poll();
                if (root.left!=null) {
                    listOfNodes.add(root.left);
                    nodes.add(root.left);
                }
                if (root.right!=null) {
                    listOfNodes.add(root.right);
                    nodes.add(root.right);
                }
            }

        }
    }
}
