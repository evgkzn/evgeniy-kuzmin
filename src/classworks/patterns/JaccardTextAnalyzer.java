package classworks.patterns;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JaccardTextAnalyzer implements TextAnalyzer{
    @Override
    public double analyze(TextProvider tp1, TextProvider tp2) {
        String text1 = tp1.getText();
        String text2 = tp2.getText();
        Set<String> uniqueWords1 = getUnique(tokenize(text1));
        Set<String> uniqueWords2 = getUnique(tokenize(text2));
        int interseptionsCount = 0;
        for (String s : uniqueWords1){
            if (uniqueWords2.contains(s)){
                interseptionsCount++;
            }
        }
        double coef = (double)interseptionsCount/(uniqueWords1.size()+uniqueWords2.size()-interseptionsCount);
        return coef;
    }

    private List<String> tokenize(String text){
        text = text.replace(",","")
                        .replace(".","")
                        .toLowerCase();
        return Arrays.asList(text.split(" "));
    }

    private Set<String> getUnique(List<String> tokens){
        return new HashSet<>(tokens);
    }
}
