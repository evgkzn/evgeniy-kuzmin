package classworks.patterns;

public class SimpleTextProvider implements TextProvider{
    private String string;
    public SimpleTextProvider(String str) {
        this.string = str;
    }

    @Override
    public String getText() {
        return this.string;
    }
}
