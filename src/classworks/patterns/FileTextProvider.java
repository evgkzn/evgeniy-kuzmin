package classworks.patterns;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileTextProvider implements TextProvider{
    private File file;

    public FileTextProvider(File file) {
        this.file = file;
    }

    @Override
    public String getText() {

        try {
            Scanner sc = new Scanner(file);
            StringBuilder sb = new StringBuilder();
            while (sc.hasNext()){
                sb.append(sc.next() + " ");
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
