package classworks.patterns;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        TextProvider tp1 = new SimpleTextProvider("Рама мама");
        File f = new File("someText.txt");
        TextProvider tp2 = new FileTextProvider(f);
        TextAnalyzer analyzer = new JaccardTextAnalyzer();
        System.out.println(analyzer.analyze(tp1, tp2));
    }
}
