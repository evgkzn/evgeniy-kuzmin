package classworks.patterns;

public interface TextProvider {
    String getText();
}
