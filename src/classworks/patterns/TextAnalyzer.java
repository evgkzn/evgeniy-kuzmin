package classworks.patterns;

public interface TextAnalyzer {
    double analyze(TextProvider tp1, TextProvider tp2);
}
