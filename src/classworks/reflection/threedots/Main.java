package classworks.reflection.threedots;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        String[] names = {"Alex", "Sam", "John"};
        printAll(names);
        printAll("Alex", "John", "Sam");
    }

//    private static void printAll(String[] names){
//        List<String> list = Arrays.asList(names);
//        list.stream()
//                .forEach(System.out::println);
//    }
private static void printAll(String...names){
      List<String> list = Arrays.asList(names);
        list.stream()
                .forEach(System.out::println);
    }
}
