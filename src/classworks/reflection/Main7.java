package classworks.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Main7 {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Student> c = Student.class;
        Constructor<Student> constructor = c.getConstructor(int.class,String.class);
        Student s = new Student(45,"John");
        Student studak = constructor.newInstance(45,"Alex");
        System.out.println(studak);

    }
}
