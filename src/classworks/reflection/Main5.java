package classworks.reflection;

public class Main5 {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        Class<Student> c = Student.class;
        Student s = c.newInstance();
        s.name = "Salavat";
        System.out.println(s);
    }
}
