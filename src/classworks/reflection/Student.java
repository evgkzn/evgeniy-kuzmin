package classworks.reflection;

public class Student {
    private int age;
    public String name;

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    private void passExam(String exam){
        System.out.println(exam + " passed");
        System.out.println("Also my name is " + name);
    }

    public Student(int age) {
        this.age = age;
    }

    private void passExam(String exam, int countofTries){
        System.out.println("Luckely passed " + exam + " from " + countofTries + " tries");
    }

    public Student(int age, String name) {
        this.age = age;
        this.name = name;
    }
}
