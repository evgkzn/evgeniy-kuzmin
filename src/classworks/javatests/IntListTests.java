//package classworks.javatests;
//
//import classworks.IntLinkedList;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
//public class IntListTests {
//
//    public IntLinkedList ill = new IntLinkedList();
//
//    @Test
//    public void testOnEmpty(){
//        assertTrue(ill.isEmpty());
//    }
//
//    @Test
//    public void testOnGetting(){
//        ill.add(1);
//        assertEquals(1,ill.get(1));
//    }
//
//    @Test(expected = IndexOutOfBoundsException.class)
//    public void testOnIndexOfRemovingWrongIndex(){
//        ill.remove(-1);
//    }
//
//    @Test(expected = IllegalStateException.class)
//    public void testOnIndexOfGetting(){
//        ill.get(-1);
//    }
//
//    @Test
//    public void testOnNotEmpty(){
//        ill.add(1);
//        assertFalse(ill.isEmpty());
//    }
//
//    @Test
//    public void testOnGetting42(){
//        ill.add(81);
//        assertFalse(ill.get(1)==42);
//    }
//
//    @Test(expected = IllegalStateException.class)
//    public void testOnRemoving(){
//        ill.add(43);
//        ill.remove(0);
//        ill.get(0);
//    }
//}
