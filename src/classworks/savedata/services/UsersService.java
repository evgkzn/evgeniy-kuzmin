package classworks.savedata.services;

import classworks.savedata.models.User;

public interface UsersService {
    User signUp(User user);
    void signIn(User user);
}
