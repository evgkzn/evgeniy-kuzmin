package classworks.savedata.models;

import classworks.savedata.dao.UserDao;
import classworks.savedata.dao.UserDaoTextFileImpl;
import classworks.savedata.services.UsersService;

public class UserServiceImpl implements UsersService {
    private UserDao userDao;

    public UserServiceImpl() {
        this.userDao = new UserDaoTextFileImpl();
    }

    @Override
    public User signUp(User user) {
        if (user.getPassword().length()<8){
            throw new IllegalArgumentException("Password should not be shorter than 8 symbols");
        }
        return userDao.save(user);
    }

    @Override
    public void signIn(User user) {

    }
}
