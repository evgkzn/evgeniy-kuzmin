package classworks.savedata.generators;

public interface IdGenerator {
    Long getNext();
}
