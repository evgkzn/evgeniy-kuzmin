package classworks.savedata.dao;

import classworks.savedata.generators.IdGenerator;
import classworks.savedata.generators.UsersIdGeneratorImpl;
import classworks.savedata.models.User;

import java.io.*;
import java.util.Scanner;

public class UserDaoTextFileImpl implements UserDao{
    private String fileName = "users_data.txt";
    private IdGenerator generator;

    public UserDaoTextFileImpl() {
        this.generator = new UsersIdGeneratorImpl();
        this.fileName = "user_data.txt";
    }

    @Override
    public User save(User model) {
        try {
            File f = new File(fileName);
            OutputStream os = new FileOutputStream(f, true);
            PrintWriter pw = new PrintWriter(os);
            Long id = generator.getNext();
            pw.println(id + " " + model.getNickname() + " " + model.getPassword());
            pw.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File with name = " + fileName + " not found");
        }
        return model;
    }

    @Override
    public User find(long id) {
        return null;
    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(long id) {

    }
}
