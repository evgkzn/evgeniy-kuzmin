package classworks.savedata.dao;

public interface CrudDao<T> {
    //Create
    T save(T model);

    //Read
    T find(long id);

    //Update
    void update(T model);

    //Delete
    void delete(long id);
}
