package classworks.savedata;

import classworks.savedata.models.User;
import classworks.savedata.models.UserServiceImpl;
import classworks.savedata.services.UsersService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Name?");
        String username = sc.nextLine();
        System.out.println("Password?");
        String password = sc.nextLine();
        User u = new User(username,password);
        UsersService service = new UserServiceImpl();
        User createdUser = service.signUp(u);
        System.out.println(createdUser);
    }
}
