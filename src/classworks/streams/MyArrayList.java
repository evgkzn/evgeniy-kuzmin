package classworks.streams;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class MyArrayList<T> {
    private T[] elements;
    private int count;

    Stream<T> stream() {
        return new MyStream();
    }

    public MyArrayList(){
        this.elements = (T[]) new Object[20];
    }

    public void add(T elem){
        elements[count++] = elem;
    }

    public class MyStream implements Stream<T> {

        public List<T> getList(){
            List<T> list = new ArrayList<>();
            for (T elem : elements){
                if (elem!=null){
                    list.add(elem);
                }
            }
            return list;
        }

        @Override
        public Stream<T> filter(Predicate<? super T> predicate) {
            List<T> list = getList();
            List<T> neededList = new ArrayList<>();
            for (T t : list){
                if(predicate.test(t)){
                    neededList.add(t);
                }
            }
            return neededList.stream();
        }

        @Override
        public <R> Stream<R> map(Function<? super T, ? extends R> mapper) {
            List<T> list = getList();
            List<R> neededList = new ArrayList<>();
            for (T elem : list){
                neededList.add(mapper.apply(elem));
            }
            return neededList.stream();
        }


        @Override
        public void forEach(Consumer<? super T> action) {
            List<T> list = getList();
            for (T element : list){
                action.accept(element);
            }
        }

        @Override
        public IntStream mapToInt(ToIntFunction<? super T> mapper) {
            return null;
        }

        @Override
        public LongStream mapToLong(ToLongFunction<? super T> mapper) {
            return null;
        }

        @Override
        public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
            return null;
        }

        @Override
        public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
            return null;
        }

        @Override
        public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
            return null;
        }

        @Override
        public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
            return null;
        }

        @Override
        public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
            return null;
        }

        @Override
        public Stream<T> distinct() {
            return null;
        }

        @Override
        public Stream<T> sorted() {
            return null;
        }

        @Override
        public Stream<T> sorted(Comparator<? super T> comparator) {
            return null;
        }

        @Override
        public Stream<T> peek(Consumer<? super T> action) {
            return null;
        }

        @Override
        public Stream<T> limit(long maxSize) {
            return null;
        }

        @Override
        public Stream<T> skip(long n) {
            return null;
        }

        @Override
        public void forEachOrdered(Consumer<? super T> action) {

        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <A> A[] toArray(IntFunction<A[]> generator) {
            return null;
        }

        @Override
        public T reduce(T identity, BinaryOperator<T> accumulator) {
            return null;
        }

        @Override
        public Optional<T> reduce(BinaryOperator<T> accumulator) {
            return Optional.empty();
        }

        @Override
        public <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner) {
            return null;
        }

        @Override
        public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner) {
            return null;
        }

        @Override
        public <R, A> R collect(Collector<? super T, A, R> collector) {
            return null;
        }

        @Override
        public Optional<T> min(Comparator<? super T> comparator) {
            return Optional.empty();
        }

        @Override
        public Optional<T> max(Comparator<? super T> comparator) {
            return Optional.empty();
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public boolean anyMatch(Predicate<? super T> predicate) {
            return false;
        }

        @Override
        public boolean allMatch(Predicate<? super T> predicate) {
            return false;
        }

        @Override
        public boolean noneMatch(Predicate<? super T> predicate) {
            return false;
        }

        @Override
        public Optional<T> findFirst() {
            return Optional.empty();
        }

        @Override
        public Optional<T> findAny() {
            return Optional.empty();
        }

        @Override
        public Iterator<T> iterator() {
            return null;
        }

        @Override
        public Spliterator<T> spliterator() {
            return null;
        }

        @Override
        public boolean isParallel() {
            return false;
        }

        @Override
        public Stream<T> sequential() {
            return null;
        }

        @Override
        public Stream<T> parallel() {
            return null;
        }

        @Override
        public Stream<T> unordered() {
            return null;
        }

        @Override
        public Stream<T> onClose(Runnable closeHandler) {
            return null;
        }

        @Override
        public void close() {

        }
    }
}
