package classworks.lambdas;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Student st1 = new Student("JS", "sdka", 19);
        Student st2 = new Student("JSfsaf", "sdkaasfas", 9);
        Comparator<Student> byAgeC = (o1, o2) -> o1.getAge()-o2.getAge();
        Comparator<Student> byNameC = (o1,o2) -> o1.getName().compareTo(o2.getName());
        Group gr = new Group(byAgeC);
        gr.add(st1);
        gr.add(st2);

        Group gr2 = new Group(byNameC);
        gr2.add(st1);
        gr2.add(st2);
    }
}
