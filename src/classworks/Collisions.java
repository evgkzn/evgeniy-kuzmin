package classworks;
import java.util.Random;
public class Collisions {
    public static void main(String[] args) {
        int[] arr = new int[16];
        for (int i = 0;i<10000;i++){
            String s = getRandomString();
            int hashCode = s.hashCode();
            int index = Math.abs(hashCode) % 16;
            arr[index]++;
        }
        int check = 0;
        for (int i = 0; i<16;i++){
            System.out.println(i + " : " + arr[i]);
            check+=arr[i];
        }
        System.out.println("Sum = " + check);
    }

    static String getRandomString(){
            String SALTCHARS = "abcdefghijklmnopqrstuvwxyz";
            StringBuilder salt = new StringBuilder();
            Random rnd = new Random();
            while (salt.length() < 5) { // length of the random string.
                int index = (int) (rnd.nextFloat() * SALTCHARS.length());
                salt.append(SALTCHARS.charAt(index));
            }
            String saltStr = salt.toString();
            return saltStr;
        }
    }
