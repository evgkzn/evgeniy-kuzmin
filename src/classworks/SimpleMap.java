package classworks;
import java.io.File;
import java.util.Scanner;

public class SimpleMap<K, V> implements Map<K, V> {
    private Entry<K, V>[] entries;
    private  int size = 10;
    private int n;
    private static final double COEFFICIENT = 1.5;

    public SimpleMap() {
        this.entries = new Entry[size];
        this.n = 0;
    }

    @Override
    public void put(K key, V value) {
        if (n == entries.length) {
            Entry<K, V>[] newArray = new Entry[(int) (entries.length * COEFFICIENT)];
            for (int i = 0; i < entries.length; i++) {
                newArray[i] = entries[i];
            }
            entries = newArray;
        }
        for (int i = 0;i<n;i++){
            if (entries[i].key.equals(key)) {
                entries[i].value = value;
                return;
            }
        }
        entries[n++] = new Entry<>(key, value);
    }

    @Override
    public V get(K key) {
        for (int i = 0;i<n;i++){
            if (entries[i].key.equals(key)){
                return entries[i].value;
            }
        }
        return null;
    }

    public void checkKeys(){
        for (int i =0;i<n;i++){
            System.out.println("Key : " + entries[i].key + " - " + entries[i].value);
        }
    }


    class Entry<I, O>{
        I key;
        O value;


        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }
    }
}
