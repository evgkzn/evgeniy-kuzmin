package classworks;

import java.sql.SQLOutput;

public class QueueGMain {
    public static void main(String[] args) {
        QueueG<Integer> qg = new QueueG<>();
        qg.enqueue(10);
        qg.enqueue(13);
        qg.enqueue(19);
        System.out.println(qg.dequeue());
        System.out.println(qg.dequeue());
        System.out.println(qg.dequeue());
    }
}
