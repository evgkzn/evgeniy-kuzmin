package task14;

public class Box2<T,V> {
    private T item;

    public V getItem2() {
        return item2;
    }

    public void setItem2(V item2) {
        this.item2 = item2;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    private V item2;

    public Box2(T o, V o2){
        item = o;
        item2 = o2;
    }
}
