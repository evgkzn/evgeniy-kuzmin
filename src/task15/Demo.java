package task15;

public class Demo {
    public static void main(String[] args) {
        WorkWithGraphs wc = new WorkWithGraphs();
        PrintGraphs pg = new PrintGraphs();
        Character[][] a1 = {{'x','*','x'},{'x','*','x'},{'x','*','x'}};
        Character[][] a2 = {{'x','x','x'},{'*','*','*'},{'x','x','x'}};
        System.out.println("First graph: ");
        pg.printGraphs(a1);
        System.out.println("Second graph: ");
        pg.printGraphs(a2);
        wc.join(a1,a2);
        wc.intersect(a1,a2);
    }
}
