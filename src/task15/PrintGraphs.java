package task15;

public class PrintGraphs {
    public void printGraphs(Character[][] a) {
        if (!(a == null)) {
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a[0].length; j++) {
                    if (a[i][j].equals(a[i][j])) {
                        System.out.print(a[i][j] + " ");
                    }
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
