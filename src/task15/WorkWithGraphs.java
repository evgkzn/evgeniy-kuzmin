package task15;

public class WorkWithGraphs {
    public Character[][] join(Character[][] a1, Character[][] a2){
        if ((a1.length==a2.length)&&(a2[0].length==a2[0].length)){
        Character[][] array = new Character[a1.length][a1[0].length];
        for(int i=0;i<a1.length;i++){
            for (int j=0;j<a1[0].length;j++){
                if(a1[i][j].equals('*')) {
                    array[i][j] = a1[i][j];
                    } else if ((a2[i][j].equals('*'))){
                    array[i][j] = a2[i][j];
                    }else {
                    array[i][j] = 'x';
                    }
                }
            }
            PrintGraphs pg = new PrintGraphs();
            System.out.println("Joined graphs: ");
            pg.printGraphs(array);
            return array;
        } else {
            System.out.println("Incorrect input. Arrays must not be with different sizes");
            return null;
        }
    }

    public Character[][] intersect(Character[][] a1, Character[][] a2){
        if ((a1.length==a2.length)&&(a2[0].length==a2[0].length)){
            Character[][] array = new Character[a1.length][a1[0].length];
            for(int i=0;i<a1.length;i++){
                for (int j=0;j<a1[0].length;j++){
                    if((a1[i][j].equals('*'))&&(a2[i][j]).equals('*')) {
                        array[i][j] = a1[i][j];
                    }else {
                        array[i][j] = 'x';
                    }
                }
            }
            PrintGraphs pg = new PrintGraphs();
            System.out.println("Intersected graphs: ");
            pg.printGraphs(array);
            return array;
        } else {
            System.out.println("Incorrect input. Arrays must not be with different sizes");
            return null;
        }
    }
}
