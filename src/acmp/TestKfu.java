package acmp;

import java.math.BigInteger;
import java.util.Scanner;

public class TestKfu {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BigInteger korobok = BigInteger.valueOf(sc.nextInt());
        BigInteger banok = BigInteger.valueOf(sc.nextInt());
        BigInteger coeffL = BigInteger.valueOf(1000000009);

        if (banok.subtract(korobok).compareTo(BigInteger.valueOf(0))<0){
            System.out.println(0);
        } else {
            try {
                System.out.println((f(banok).divide((f(banok.subtract(korobok)).multiply(f(korobok))))).mod(coeffL));
            } catch (ArithmeticException e){
                System.out.println(0);
            }
        }

    }

    public static BigInteger f(BigInteger n){
        if (n.compareTo(BigInteger.valueOf(0))==0){
            return BigInteger.valueOf(1);
        } else if (n.compareTo(BigInteger.valueOf(1))==0){
            return BigInteger.valueOf(1);
        }else {
            return f(n.subtract(BigInteger.valueOf(1))).multiply(n);
        }
    }

}
