package acmp;

import java.io.*;
import java.util.*;

public class Task41 {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");
        Scanner sc = new Scanner(file);
        int howMany = Integer.parseInt(sc.nextLine());
        int[] array = new int[201];
        while (sc.hasNextInt()){
            array[sc.nextInt()+100]++;
        }

        FileWriter fw = new FileWriter("output.txt", false);
        BufferedWriter bw = new BufferedWriter(fw, howMany);

        for (int i = 0; i<array.length;i++) {
            while (array[i]>0) {
                bw.write((i-100) + " ");
                array[i]--;
            }
        }
        bw.flush();
        bw.close();
    }
}
