package acmp;

public class K {
    public static void main(String[] args) {
        System.out.println(count(8));

    }

    public static int count(int x){
        if (x == 0) return 0;
        if (x == 1) return 0;
        if (x == 2) return 0;
        if (x == 3) return 1;

        return count(x-1)+count(x-2)+count(x-3)+count(x-4);
    }
}
