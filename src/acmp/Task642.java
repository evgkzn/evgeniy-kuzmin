package acmp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Task642 {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");
        Scanner sc = new Scanner(file);
        String firstString = sc.nextLine();
        int howManyCars = Integer.parseInt(firstString.substring(0,firstString.indexOf(' ')));
        int money = Integer.parseInt(firstString.substring(firstString.indexOf(' ')+1,firstString.length()));
        String secondString = sc.nextLine();
        String[] array = secondString.split(" ");
        Integer[] arrayInt = new Integer[howManyCars];
        for (int i = 0; i<howManyCars;i++) {
            arrayInt[i] = Integer.parseInt(array[i]);
        }
        Arrays.sort(arrayInt);
        int counter = 0;
            for (Integer int1 : arrayInt) {
                if (money-int1>=0){
                    money-=int1;
                    counter++;
                } else {
                    break;
                }
            }
            writeToFile(counter);
    }
    public static <T> void writeToFile(T t) throws IOException {
        try (FileWriter fw = new FileWriter("output.txt")){
            fw.write(String.valueOf(t));
        }catch (Exception exc){
            exc.printStackTrace();
        }
    }
}
