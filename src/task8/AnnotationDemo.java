package task8;

public class AnnotationDemo {
    public static void main(String[] args) {
        Table t = new Table();
        Class sizesTable = t.getClass();
        Sizes sTDemo = (Sizes)sizesTable.getAnnotation(Sizes.class);
        System.out.println("Table's height, width, length = " + sTDemo.height()+","+sTDemo.width()+","+sTDemo.length());
    }
}