package task8;

public class CatDemo {
    public static void main(String[] args) {
        Animal a = new Animal();
        Cat c = new Cat();
        Dog d = new Dog();
        c.speak();
        c.soundsGood();
        d.speak();
        printClassName(d);
    }

    public static void printClassName(Object obj) {
        System.out.println("The class of " + obj +
                " is " + obj.getClass().getName());
    }
}
