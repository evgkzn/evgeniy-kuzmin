package task1;

public class Triangle {
    private int sideA;
    private int sideB;
    private int sideC;

    public int getSideA() {
        return sideA;
    }

    public void setSideA(int sideA) {
        this.sideA = sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public void setSideB(int sideB) {
        this.sideB = sideB;
    }

    public int getSideC() {
        return sideC;
    }

    public void setSideC(int sideC) {
        this.sideC = sideC;
    }

    private int perimeter;
    public void findPerimeter() {
        perimeter = sideA + sideB + sideC;
        System.out.println("Triangle perimeter: " + perimeter);
    }

    public void findSquare() {
        double square = Math.sqrt(0.5 * perimeter * (0.5 * perimeter - sideA) * (0.5 * perimeter - sideB) * (0.5 * perimeter - sideC));
        System.out.println("Triangle square: " + square);
    }
}
