package task1;

public class RectangularParallelepiped {

    private int height;
    private int width;
    private int depth;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void findSquare() {
        int square = 2 * (height * width + height * depth + width * depth);
        System.out.println("Rectangular Parallelepiped square: " + square);
    }

    public void findCapacity() {
        int capacity = height * width * depth;
        System.out.println("Rectangular parallelepiped capacity: " + capacity);
    }

    public void findDiagonal() {
        double diagonal = Math.sqrt(height*height+width*width+depth*depth);
        System.out.println("Rectangular parallelepiped diagonal: " + Math.rint(diagonal));
    }


}
