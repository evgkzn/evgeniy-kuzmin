package task1;

public class Sphere {
    private double pi = Math.PI;
    private int rad;

    public int getRadius() {
        return rad;
    }

    public void setRadius(int radius) {
        this.rad = radius;
    }

    public void findSquare() {
        double square  = 4 * pi * rad * rad;
        System.out.println("Sphere square: " + Math.rint(square));
    }

    public void findCapacity() {
        double capacity = (4 * pi * rad * rad * rad) / 3;
        System.out.println("Sphere capacity: " + Math.rint(capacity));
    }

}
