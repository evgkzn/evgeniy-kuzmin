package task1;

public class Rectangle {

    private int height;
    private int width;

    public int getHeight() {
        return height;
    }

    public void setHeight(int a) {
        this.height = a;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int b) {
        this.width = b;
    }

    public void findPerimeter() {
        int perimeter = 2 * (getHeight() + getWidth());
        System.out.println("Rectangle perimeter: " + perimeter);
    }

    public void findSquare() {
        int square = width * height;
        System.out.println("Rectangle square: " + square);
    }

}
