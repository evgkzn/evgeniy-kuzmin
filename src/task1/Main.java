package task1;

public class Main {
    public static void main(String[] args) {
        Line ln = new Line();
        ln.setLength(10);
        System.out.println("Line length: " + ln.getLength());
        System.out.println();

        Window wn = new Window();
        wn.setClosed(true);
        System.out.println("Is window closed? -  " + wn.isClosed());
        System.out.println();

        Rectangle rc = new Rectangle();
        rc.setHeight(10);
        rc.setWidth(5);
        System.out.println("Rectangle width : " + rc.getWidth() + " height " + rc.getHeight());
        rc.findPerimeter();
        rc.findSquare();
        System.out.println();

        Circle cc = new Circle(5);
        System.out.println("Circle radius: " + cc.getRad());
        cc.findPerimeter();
        cc.findSquare();
        System.out.println();

        Triangle tr = new Triangle();
        tr.setSideA(3);
        tr.setSideB(4);
        tr.setSideC(5);
        System.out.println("Triagnle sideA: " + tr.getSideA() + " sideB: " + tr.getSideB() + " sideC: " + tr.getSideC());
        tr.findPerimeter();
        tr.findSquare();
        System.out.println();

        RectangularParallelepiped rp = new RectangularParallelepiped();
        rp.setHeight(3);
        rp.setWidth(4);
        rp.setDepth(5);
        System.out.println("Rectangular Parallelepiped height: " + rp.getHeight() + " width: " + rp.getWidth() + " depth: " + rp.getDepth());
        rp.findSquare();
        rp.findCapacity();
        rp.findDiagonal();
        System.out.println();

        Sphere sp = new Sphere();
        sp.setRadius(5);
        System.out.println("Sphere radius: " + sp.getRadius());
        sp.findSquare();
        sp.findCapacity();
        System.out.println();


    }
}
