package task1;

public class Circle {
   private double pi = Math.PI;

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public Circle(int rad) {
        this.rad = rad;
    }

    private int rad;

    public void findPerimeter() {
        double perimeter = 2 * pi * rad;
        System.out.println("Circle perimeter: " + Math.rint(perimeter));
    }

    public void findSquare() {
        double square = pi * rad * rad;
        System.out.println("Circle square: " + Math.rint(square));
    }

}
