package tasks;
import java.util.Scanner;

public class N11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int res = 1;
        int i;
        if (n%2==0) {
            i=2;
            while (i<=n) {
                res = res*i;
                i = i + 2;
            }
        } else {
            i=1;
            while (i<=n) {
                res = res*i;
                i = i + 2;
            }
        }
        System.out.println("Answer = " + res);
    }
}
