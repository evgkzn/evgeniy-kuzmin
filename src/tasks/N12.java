package tasks;
import java.util.Scanner;
public class N12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double m = 1;
        double k = 3;
        double res = 0;
        double term = 1/(m*m) - 1/(k*k);
        for (int i=1;i<=n;i++) {
            res = res + term;
            m = m+4;
            k = k+4;
        }
        System.out.println("Answer = " + res);
    }
}
