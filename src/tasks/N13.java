package tasks;
import java.util.Scanner;
public class N13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double n = sc.nextInt();
        double res = 1;

        for (int i = 1; i<=n;i++) {
            res = res*findTerm(i);
        }
        System.out.println("Answer = " + res);
    }
    public static double term;
    public static double findTerm(double l) {
        term = (2 * l / (2 * l - 1)) * (2 * l / (2 * l + 1));
        return term;
    }
}