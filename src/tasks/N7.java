package tasks;

public class N7 {
    public static void main(String[] args) {
        int a = 2;
        int b = 1;

        System.out.println("a + b = " + (a+b));
        System.out.println("a - b = " + (a-b));
        System.out.println("b - a = " + (b-a));
        System.out.println("a * b = " + (a*b));
        System.out.println("a / b = " + (a/b));
        System.out.println("a % b = " + (a%b));
        System.out.println("b / a = " + (b/a));
        System.out.println("b % a = " + (b%a));

    }
}
