package homeworks;

public class QueueDemo {
    public static void main(String[] args) {
        Queue q = new Queue();
        q.enqueue(12);
        q.enqueue(10);
        q.enqueue(93);
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
        System.out.println(q.dequeue());
    }
}
