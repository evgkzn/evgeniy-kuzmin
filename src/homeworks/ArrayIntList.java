package homeworks;

import classworks.IntLinkedList;

public class ArrayIntList implements IntList {
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICIENT = 1.5;
    private int[] arr;
    private int n;
    private int first = 0;

    public ArrayIntList() {
        this.arr = new int[INITIAL_CAPACITY];
        this.n = 0;
    }

    @Override
    public void add(int elem) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
        arr[n++] = elem;
    }

    @Override
    public void add(int elem, int position) {
        if (arr[position] == 0) {
            arr[position] = elem;
        } else {
            if (n == arr.length) {
                int[] newArray = new int[(int) (arr.length * COEFFICIENT)];
                for (int i = 0; i < arr.length; i++) {
                    newArray[i] = arr[i];
                }
                arr = newArray;
            }
            int nextelem = arr[position];
            arr[position] = elem;
            int prevelem;
            for (int i = ++position; i < arr.length; i++) {
                prevelem = arr[i];
                arr[i] = nextelem;
                nextelem = prevelem;
            }
        }
    }


    @Override
    public int get(int index) {
        return arr[index];
    }

    @Override
    public int remove(int index) {
        int delint = arr[index];
        arr[index] = 0;
        return delint;
    }

    @Override
    public int size() {
        int size = 0;
        for (int i : arr) {
            if (i != 0) {
                size++;
            }
        }
        return size;
    }

    @Override
    public int[] toArray() {
        int[] newarr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            newarr[i] = arr[i];
        }
        return newarr;
    }

    @Override
    public void sort() {
        boolean swapped = false;
        do {
            swapped = false;
            for (int i = 0; i < arr.length - 1; i++) {
                if ((arr[i] > arr[i + 1]) && (arr[i] != 0) && (arr[i + 1] != 0)) {
                    int perm = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = perm;
                    swapped = true;
                }
            }
        } while (swapped != false);
    }

    @Override
    public void addAll(IntList list, int position) {
        int counter = 0;
        int steps = 0;
        int momentPosition = position;
        while (counter != list.size()) {
            if (list.get(steps) != 0) {
                add(list.get(steps), momentPosition);
                steps++;
                momentPosition++;
                counter++;
            }

        }

    }

    @Override
    public int lastIndexOf(int elem) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == elem) {
                return i;
            }
        }
        throw new IllegalStateException("Wrong elem");
    }

    @Override
    public IntIterator iterator() {
        return new ArrayIntIteratorImpl();
    }

    class ArrayIntIteratorImpl implements IntIterator {
        int current;

        public ArrayIntIteratorImpl() {
            current = first;
        }

        @Override
        public boolean hasNext() {
            return arr[current]!=0;
        }

        @Override
        public int next() {
            int valueToReturn = arr[current];
            current++;
            return valueToReturn;
        }
    }
}

