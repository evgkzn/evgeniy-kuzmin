package homeworks.SortedSimpleMap;

import classworks.Map;

import java.util.Comparator;

public class SortedSimpleMap<K, V> implements Map<K, V> {
    private Entry<K, V>[] entries;
    private int size = 10;
    private int n;
    private static final double COEFFICIENT = 1.5;
    private Comparator<K> comparator;

    public SortedSimpleMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    @Override
    public void put(K key, V value) {
        if (n == entries.length) {
            Entry<K, V>[] newArray = new Entry[(int) (entries.length * COEFFICIENT)];
            for (int i = 0; i < entries.length; i++) {
                newArray[i] = entries[i];
            }
            entries = newArray;
        }
        int startingPos = 0;
        int endingPos = entries.length - 1;
        while (startingPos <= endingPos) {
            int i = (startingPos + endingPos) / 2;
            if (entries[i].equals(key)) {
                entries[i].value = value;
                break;
            }
            else if (comparator.compare(key,entries[i].key)<0) {
                endingPos = i - 1;
            }
            else if (comparator.compare(key, entries[i].key)>0) {
                startingPos = i + 1;
            }
        }
        entries[n++] = new Entry<>(key, value);
    }

    @Override
    public V get(K key) {
        for (int i = 0;i<n;i++){
            if (entries[i].key.equals(key)){
                return entries[i].value;
            }
        }
        return null;
    }


    class Entry<I, O> {
        I key;
        O value;

        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }
    }
}

