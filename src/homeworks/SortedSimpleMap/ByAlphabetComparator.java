package homeworks.SortedSimpleMap;

import java.util.Comparator;

public class ByAlphabetComparator implements Comparator<Entry> {

    @Override
    public int compare(Entry o1, Entry o2) {
        return o1.key.toString().compareTo(o2.key.toString());
    }
}
