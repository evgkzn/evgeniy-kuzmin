package homeworks.SortedSimpleMap;

class Entry<I, O> {
    I key;
    O value;

    public Entry(I key, O value) {
        this.key = key;
        this.value = value;
    }
}
