package homeworks;

public class StackLinkedList {
    private Node first;
    private int n;

    public void push(int elem){
        Node newNode = new Node();
        newNode.value = elem;
        if (first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        n++;
    }

    public int pop(){
        if (n<=0){
            throw new IllegalStateException("Stack is empty");
        }
        int counter = 1;
        Node neededValueNode;
        Node neededPrevNode;
        if (first != null) {
            neededValueNode = first;
            while (counter != n) {
                neededValueNode = neededValueNode.next;
                counter++;
            }

            counter = 1;
            neededPrevNode = first;
            while (counter != (n-1)) {
                if (n==1){
                    break;
                }
                neededPrevNode = neededPrevNode.next;
                counter++;
            }
            neededPrevNode.next = null;
            n--;
            return neededValueNode.value;
        }
        throw new IllegalStateException("Wrong index.");


    }

    public boolean isEmpty(){
        return n==0;
    }


    class Node {
        int value;
        Node next;
    }
}
