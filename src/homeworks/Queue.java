package homeworks;

public class Queue {
    private Node first;
    private int n;

    public void enqueue(int elem){
        Node newNode = new Node();
        newNode.value = elem;
        if (first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        n++;
    }

    public int dequeue(){
        if (n==0){
            throw new IllegalStateException("Queue is empty");
        }
        Node exitNode = first;
        first = first.next;
        n--;
        return exitNode.value;
    }

    public boolean isEmpty(){
        return n==0;
    }

    class Node {
        int value;
        Node next;
    }
}
