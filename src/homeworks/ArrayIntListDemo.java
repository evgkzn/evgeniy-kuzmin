package homeworks;

public class ArrayIntListDemo {
    public static void main(String[] args) {
        ArrayIntList ar = new ArrayIntList();
        ar.add(1);
        ar.add(2);
        ar.add(3);
        ar.add(4);
        ArrayIntList br = new ArrayIntList();
        br.add(7);
        br.add(7);
        br.add(5);

        ar.addAll(br,1);
        int[] arr1 = ar.toArray();
        for (int i = 0;i<arr1.length;i++){
            System.out.print(arr1[i] + " ");
        }
        System.out.println();
        ar.sort();
        int[] arr2 = ar.toArray();
        for (int i = 0;i<arr1.length;i++){
            System.out.print(arr2[i] + " ");
        }

        IntIterator iter = ar.iterator();
        int sum = 0;
        while (iter.hasNext()) {
            sum += iter.next();
        }
        iter = ar.iterator();
        System.out.println(sum);
    }
}
