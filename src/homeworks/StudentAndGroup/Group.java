package homeworks.StudentAndGroup;

import homeworks.StudentAndGroup.Student;

import java.util.Comparator;

public class Group {
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICIENT = 1.5;
    private Student[] group;
    private int n;
    private int first = 0;
    Comparator<Student> comparator;

    public Group(){
        this.group = new Student[INITIAL_CAPACITY];
        this.n = 0;
    }
    public Group(Comparator<Student> comparator){
        this();
        this.comparator = comparator;

    }

    public void add(Student student){
        if (n==0){
            group[0] = student;
        } else {
            int i = 0;
            while (i<n){
                if (comparator!=null){
                    if (comparator.compare(student,group[i])<0){
                        break;
                    }
                } else {
                    if (student.compareTo(group[i])<0){
                        break;
                    }
                }
                i++;
            }

            if (n == group.length) {
                Student[] newArray = new Student[(int) (group.length * COEFFICIENT)];
                for (int k = 0; k < group.length; k++) {
                    newArray[k] = group[k];
                }
                group = newArray;
            }

            Student nextelem = group[i];
            group[i] = student;
            Student prevelem;
            for (int j = ++i; j < group.length; j++) {
                prevelem = group[j];
                group[j] = nextelem;
                nextelem = prevelem;
            }
        }
        n++;
    }
}
