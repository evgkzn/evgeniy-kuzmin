package homeworks.StudentAndGroup;

public class Main {
    public static void main(String[] args) {
        ByNameComparator comparator = new ByNameComparator();
        Group gr = new Group(comparator);
        Student st1 = new Student("Evgeniy", "Kuzmin", 18);
        Student st2 = new Student("Lenar", "Shaikhiev", 19);
        Student st3 = new Student("Egor", "Ksenofontov", 18);
        gr.add(st1);
        gr.add(st2);
        gr.add(st3);
    }
}
