package homeworks.StudentAndGroup;

public class Student implements Comparable<Student>{
    public Student(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }


    public int getAge() {
        return age;
    }

    private String name;
    private String surname;
    private int age;

    @Override
    public boolean equals(Object object){
        if (object==null){
            return false;
        }
        if (!(object instanceof Student)){
            return false;
        }
        if(this == object){
            return true;
        }
        Student st = (Student) object;
        return this.getName() == st.getName() &&
                this.getSurname() == st.getSurname() &&
                this.getAge() == st.getAge();
    }

    @Override
    public int compareTo(Student o) {
        return this.getSurname().compareTo(o.getSurname());
    }
}
