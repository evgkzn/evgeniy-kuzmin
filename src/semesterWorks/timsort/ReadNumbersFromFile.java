package semesterWorks.timsort;

import java.io.*;
import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.Scanner;

public class ReadNumbersFromFile {
    public Integer[] readSetOfNumbers(File fl, int string) throws IOException {
            Integer[] neededIntArray = null;
            int counter = 1;
            FileReader fr = new FileReader(fl);
            Scanner sc = new Scanner(fr);
            boolean isOk = false;
            String neededString;
            while (!isOk) {
                if (string == counter) {
                    neededString = sc.nextLine();
                    neededString = neededString.trim();
                    String[] neededStringArray = neededString.split(" ");
                    neededIntArray = new Integer[neededStringArray.length];
                    for (int i = 0; i < neededStringArray.length; i++) {
                        neededIntArray[i] = Integer.parseInt(neededStringArray[i]);
                    }
                    isOk = true;
                } else {
                    neededString = sc.nextLine();
                    counter++;
                }
            }
            return neededIntArray;
        }

    public LinkedList<Integer> readSetOfNumbers(File fl, int string, int mode) throws IOException {
            LinkedList<Integer> neededLinkedList = new LinkedList<Integer>();
            int counter = 1;
            FileReader fr = new FileReader(fl);
            Scanner sc = new Scanner(fr);
            boolean isOk = false;
            String neededString;
            while (!isOk) {
                if (string == counter) {
                    neededString = sc.nextLine();
                    neededString = neededString.trim();
                    String[] neededStringArray = neededString.split(" ");
                    for (int i = 0; i < neededStringArray.length; i++) {
                        neededLinkedList.add(Integer.parseInt(neededStringArray[i]));
                    }
                    isOk = true;
                } else {
                    neededString = sc.nextLine();
                    counter++;
                }
            }
            return neededLinkedList;
        }
}

