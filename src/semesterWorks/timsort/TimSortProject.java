package semesterWorks.timsort;

import java.util.LinkedList;

public class TimSortProject {
    private ArrayComparator comparator = new ArrayComparator();

    public long sort(Integer[] array) {
        long startTime;
        long endTime;
        long resultTime = -1;
        startTime = System.currentTimeMillis();
        TimSort.sort(array, this.comparator);
        endTime = System.currentTimeMillis();
        resultTime = endTime-startTime;
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        return resultTime;
    }

    public long sort(LinkedList<Integer> linkedList) {
        long startTime;
        long endTime;
        long resultTime = -1;
        Integer[] array = new Integer[linkedList.size()];
        for (int i = 0; i < linkedList.size(); i++) {
            array[i] = linkedList.get(i);
        }
        startTime = System.currentTimeMillis();
        TimSort.sort(array, this.comparator);
        endTime = System.currentTimeMillis();
        resultTime = endTime - startTime;
        for (int i = 0; i < array.length; i++) {
            linkedList.add(i,array[i]);
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        return resultTime;
    }
}
