package semesterWorks.timsort;
import java.io.*;
import java.util.Random;

public class RandomNumbersGeneration{
    private final static int HOWMANY_ARRAYS = 100;
    static Random rn = new Random();
    File fl = null;

    public RandomNumbersGeneration(File fl) {
        this.fl = fl;
    }

    public void generateNewNumbers() throws IOException {
        RandomNumbersGeneration rng = new RandomNumbersGeneration(fl);
        rng.cleanFile();
        int howManyNumbersInside = 0;
        for (int i = 0; i < HOWMANY_ARRAYS; i++) {
            howManyNumbersInside +=100;
            for (int j = 0; j < howManyNumbersInside; j++) {
                int number = rn.nextInt(99)+1;
                String toFile = Integer.toString(number);
                rng.writeToFile(toFile);
            }
            rng.writeToFile("\n");
        }

    }

    public void writeToFile(String string) throws IOException {
        FileWriter fw = new FileWriter(fl,true);
        fw.write(string + " ");
        fw.close();
    }

    public void cleanFile() throws IOException{
        FileWriter fw = new FileWriter(fl,false);
        fw.write("");
        fw.close();
    }

}
