package semesterWorks.timsort;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("inputNumbers.txt");
        // Если нужно сгенерировать новые данные - убери комментарии

        //
        //RandomNumbersGeneration randomNumbersGeneration = new RandomNumbersGeneration(file);
        //randomNumbersGeneration.generateNewNumbers();


        ReadNumbersFromFile readNumbersFromFile = new ReadNumbersFromFile();
        TimSortProject timSortProject = new TimSortProject();
        System.out.println("Time of sorting : " + timSortProject.sort(readNumbersFromFile.readSetOfNumbers(file, 100)));
    }
}
