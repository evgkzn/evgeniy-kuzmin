package semesterWorks.timsort;

import java.util.Comparator;

public class ArrayComparator implements Comparator<Number> {

    @Override
    public int compare(Number o1, Number o2) {
        if (o1.doubleValue()-o2.doubleValue()<0){
            return -1;
        } else if (o1.doubleValue()-o2.doubleValue()>0){
            return 1;
        } else return 0;
    }
}
