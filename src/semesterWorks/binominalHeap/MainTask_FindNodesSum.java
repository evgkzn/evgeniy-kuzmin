package semesterWorks.binominalHeap;

import java.util.Random;

public class MainTask_FindNodesSum {
    public static void main(String[] args) {
        BinomialHeap bh = new BinomialHeap();
        Random rn = new Random();
        int randomNumber;
        for (int i = 0;i<100;i++){
            randomNumber = rn.nextInt(100);
            bh.insert(randomNumber);
        }
        System.out.println(bh.findNodesSum());
    }
}
