package semesterWorks.binominalHeap;

import java.io.*;

public class MainTests {
    public static void main(String[] args) throws IOException {
        BinomialHeap bh = new BinomialHeap();
        ReadNumbersFromFile newInput = new ReadNumbersFromFile();
        File inputFile = new File("inputNumbers.txt");
        TestingClass tc = new TestingClass();
        for (int i =1; i<101;i++) {
            Integer[] inputArray = newInput.readSetOfNumbers(inputFile, i);
             bh = tc.insertingTest(inputArray);
             tc.searchingTest(bh, inputArray);
             tc.deletingTest(bh,inputArray);
        }
    }
}
