package semesterWorks.binominalHeap;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ReadNumbersFromFile {
    public Integer[] readSetOfNumbers(File fl, int string) throws IOException {
            Integer[] neededIntArray = null;
            int counter = 1;
            FileReader fr = new FileReader(fl);
            Scanner sc = new Scanner(fr);
            boolean isOk = false;
            String neededString;
            while (!isOk) {
                if (string == counter) {
                    neededString = sc.nextLine();
                    neededString = neededString.trim();
                    String[] neededStringArray = neededString.split(" ");
                    neededIntArray = new Integer[neededStringArray.length];
                    for (int i = 0; i < neededStringArray.length; i++) {
                        neededIntArray[i] = Integer.parseInt(neededStringArray[i]);
                    }
                    isOk = true;
                } else {
                    neededString = sc.nextLine();
                    counter++;
                }
            }
            return neededIntArray;
        }
}

