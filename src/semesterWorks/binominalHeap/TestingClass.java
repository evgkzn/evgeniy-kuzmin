package semesterWorks.binominalHeap;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestingClass {

    public BinomialHeap insertingTest(Integer[] array) throws FileNotFoundException {
        File outputFile = new File("insertingTestsOutput.txt");
        OutputStream os = new FileOutputStream(outputFile,true);
        PrintWriter pw = new PrintWriter(os);
        long start = 0;
        long finish = 0;
        long result = 0;
        long timeSum = 0;
        BinomialHeap bh = new BinomialHeap();
        for (int i : array){
            start = System.nanoTime();
            bh.insert(i);
            finish = System.nanoTime();
            result = finish-start;
            timeSum = timeSum + result;
        }
        pw.println(timeSum);
        pw.close();
        return bh;
    }

    public void searchingTest(BinomialHeap bh, Integer[] array) throws FileNotFoundException {
        File outputFile = new File("findingMinimumTestsOutput.txt");
        OutputStream os = new FileOutputStream(outputFile,true);
        PrintWriter pw = new PrintWriter(os);
        long start = 0;
        long finish = 0;
        long result = 0;
        long sum = 0;
        for (int i =0; i<100;i++){
            start = System.nanoTime();
            bh.findMinimum();
            finish = System.nanoTime();
            result = finish-start;
            sum = sum + result;
        }
        pw.println(sum/100);
        pw.close();
    }

    public void deletingTest(BinomialHeap bh, Integer[] array) throws FileNotFoundException {
        File outputFile = new File("deletingTestsOutput.txt");
        OutputStream os = new FileOutputStream(outputFile,true);
        PrintWriter pw = new PrintWriter(os);
        long start = 0;
        long finish = 0;
        long result = 0;
        long sum = 0;
        List<Integer> arrayList = new ArrayList<>();
        for (Integer i : array){
            arrayList.add(i);
        }
        Random rn = new Random();
        int idOfRandomElement;
        for (int i = 0;i<10;i++){
            idOfRandomElement = rn.nextInt(arrayList.size()-1);
            start = System.nanoTime();
            bh.delete(arrayList.get(i));
            finish = System.nanoTime();
            result = finish - start;
            sum = sum + result;
            arrayList.remove(i);
        }
        pw.println(sum/10);
        pw.close();

    }


}
