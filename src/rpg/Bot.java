package rpg;
import java.util.concurrent.ThreadLocalRandom;
public class Bot extends Character{
    private Artifact botsArt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bot(String name) {
        super(name);
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getOriginalHP() {
        return originalHP;
    }

    public void setOriginalHP(int originalHP) {
        this.originalHP = originalHP;
    }

    public int getOriginalStrength() {
        return originalStrength;
    }

    public void setOriginalStrength(int originalStrength) {
        this.originalStrength = originalStrength;
    }

    public int getOriginalArmor() {
        return originalArmor;
    }

    public void setOriginalArmor(int originalArmor) {
        this.originalArmor = originalArmor;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    private int originalArmor = 0;
    private int originalStrength = 10;
    private int originalHP  = 100;

    private boolean isAlive = true;
    private int armor = 0;
    private int level = 1;
    private int strength = 10;
    private double maxHit;
    private double minHit;
    private int hp = 100;
    private String name;

    public double getMinHit() {
        minHit = strength*0.5+level;
        return minHit;
    }

    public double getMaxHit(){
        maxHit = strength*1+level;
        return maxHit;
    }

    public void info() {
        System.out.println("Bot's name: " + name);
        System.out.println("----------------------------");
        System.out.println("Level: " + level);
        System.out.println("HP: " + hp);
        System.out.println("Strength: " + strength);
        System.out.println("Armor: " + armor);
        System.out.println("----------------------------");
    }


    public Artifact checkForDrop(Bot bot) {
            if (botsArt != null) {
                int i = ThreadLocalRandom.current().nextInt(1, 4);
                if (i == 1) {
                    System.out.println();
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    System.out.println(botsArt.getType() + " dropped from "+ bot.name);
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    System.out.println();
                    Artifact drop = botsArt;
                    botsArt = null;
                    recalcStats();
                    return drop;
                }
            }
        System.out.println();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("         Bad luck, no drop");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println();
            return null;
    }

    public void addArtifact(Artifact artifact) {
        switch (artifact.getType()){
            case "Weapon":
                botsArt = artifact;
                break;
            case "Helmet":
                botsArt = artifact;
                break;
            case "Chestplate":
                botsArt = artifact;
                break;
            case "Boots":
                botsArt = artifact;
                break;
            case "Shield":
                botsArt = artifact;
                break;
        }
        recalcStats();
    }

    private void recalcStats() {
        if (isAlive==true) {
            hp = originalHP;
        }
        strength = originalStrength;

            armor = originalArmor;
            if (botsArt!=null) {
                hp = hp + botsArt.getHp();
                armor = armor + botsArt.getArmor();
                strength = strength + botsArt.getStrength();
            }
    }

    public void checkArtifacts() {
        System.out.println();
        System.out.println(name + "'s artifacts: ");
            if(botsArt!=null) {
                System.out.println(botsArt.getType());
            }
    }

}

