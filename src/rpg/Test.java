package rpg;

public class Test {
    public static void main(String[] args) {
        Character ch1 = new Character("Evgeniy");
        Character ch2 = new Character("Lenar");
        Fight f = new Fight();
        f.fight(ch1,ch2);
        ch1.info();
        ch2.info();
    }
}
