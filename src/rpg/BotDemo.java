package rpg;

public class BotDemo {
    public static void main(String[] args) {
        Fight f = new Fight();
        Hit h = new Hit();

        Character ch = new Character("Evgeniy");
        Bot b = new Bot("Orc");
        Artifact club = new Artifact("Weapon", 5, 1,0);
        b.addArtifact(club);
        b.info();
        Artifact ironShield = new Artifact("Shield", 10, 1, 1);
        ch.addArtifact(ironShield);
        ch.info();
        Artifact knife = new Artifact("Weapon", 1,3,3);
        ch.addArtifact(knife);
        ch.info();
        f.fight(ch,b);
        ch.info();
        if (b.getHp()<=0) {
            Artifact art = b.checkForDrop(b);
        }
    }
}
