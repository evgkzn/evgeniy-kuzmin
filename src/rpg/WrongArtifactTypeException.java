package rpg;

public class WrongArtifactTypeException extends RuntimeException{
    public WrongArtifactTypeException(String ms) {
        super(ms);
    }
}
