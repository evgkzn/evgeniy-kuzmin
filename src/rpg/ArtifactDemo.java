package rpg;

public class ArtifactDemo {
    public static void main(String[] args) {
        Character c = new Character("Evgeniy");
        Artifact art = new Artifact("Weapon",0,5,0);
        Artifact art2 = new Artifact("Chestplate", 10, 0, 0);
        Artifact art3 = new Artifact("Shield",0,0,5);
        c.addArtifact(art);
        c.addArtifact(art2);
        c.addArtifact(art3);
        c.info();
    }
}
