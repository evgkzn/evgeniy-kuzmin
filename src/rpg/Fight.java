package rpg;

public class Fight {
    private boolean isFightGoing = false;

    public void fight(Character c1, Character c2) {
        Hit h = new Hit();
        GetHit gh = new GetHit();
        isFightGoing = true;
        System.out.println("Fight started! *Clank*");
        while (isFightGoing == true) {
            System.out.println("==|GAME STATS: " + c1.getName() + "'s HP: " + c1.getHp() + " " + c2.getName() + "'s HP: " + c2.getHp() + " |==");
            System.out.println(c1.getName() + "'s attack");
            gh.getHit(c2, h.hit(c1));
            if ((c1.getHp() <= 0) | (c2.getHp() <= 0)) {
                isFightGoing = false;
                if (c1.getHp()<0) {
                    c1.setHp(0);
                } else if (c2.getHp()<0) {
                    c2.setHp(0);
                }
                c1.setLastHP(c1.getHp());
                c2.setAlive(false);
                System.out.println();
                System.out.println(c2.getName() + " DIED ---> " + c1.getName() + " WIN");
                System.out.println("GAME OVER");
                break;
            } else {
                System.out.println("==|GAME STATS: " + c1.getName() + "'s HP: " + c1.getHp() + " " + c2.getName() + "'s HP: " + c2.getHp() + " |==");
                System.out.println();
            }

            System.out.println(c2.getName() + "'s attack");
            gh.getHit(c1, h.hit(c2));
            if ((c1.getHp() <= 0) | (c2.getHp() <= 0)) {
                isFightGoing = false;
                if (c1.getHp()<0) {
                    c1.setHp(0);
                } else if (c2.getHp()<0) {
                    c2.setHp(0);
                }
                c2.setLastHP(c2.getHp());
                c1.setAlive(false);
                System.out.println();
                System.out.println(c1.getName() + " DIED ---> " + c2.getName() + " WIN");
                System.out.println("GAME OVER!");
            } else {
                System.out.println("==|GAME STATS: " + c1.getName() + "'s HP: " + c1.getHp() + " " + c2.getName() + "'s HP: " + c2.getHp() + " |==");
                System.out.println();
            }
        }
    }
}
