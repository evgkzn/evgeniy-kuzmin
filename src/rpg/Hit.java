package rpg;
import java.util.concurrent.ThreadLocalRandom;
public class Hit {
    public int hit(Character c) {
        double hitDouble = ThreadLocalRandom.current().nextDouble(c.getMinHit(),c.getMaxHit());
        int hitInt = (int) Math.rint(hitDouble);
        return hitInt;
    }
}
