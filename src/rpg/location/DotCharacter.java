package rpg.location;

public class DotCharacter {
    public DotCharacter(int x, int y, String[][] array) {
        this.x = x;
        this.y = y;
        array[x][y] = symb;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    private int x;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    private int y;

    public String getSymb() {
        return symb;
    }

    private String symb = "★";
}
