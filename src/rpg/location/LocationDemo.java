package rpg.location;
import java.util.Scanner;
public class LocationDemo {
    private static boolean isGoing = true;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        CreateLocation crl = new CreateLocation();
        DisplayLocation dpl = new DisplayLocation();
        Location loc1 = new Location(crl.CreateLocation(10,10));
        DotBot db = new DotBot(loc1);
        DotCharacter dc = new DotCharacter(loc1.getChStartX(),loc1.getChStartY(),loc1.getLocation());
        dpl.DisplayLocation(loc1);
        DotCharacterMovement dcm = new DotCharacterMovement(dc,loc1);
        while (isGoing) {
            System.out.println("W/A/S/D?");
            String input = sc.nextLine();
            if (input.equals("Quit")) {
                break;
            } else {
                dcm.move(input);
                loc1.CheckForBattle(dc,db);
            }
        }





    }
}
