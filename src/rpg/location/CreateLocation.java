package rpg.location;

import rpg.WrongArtifactTypeException;

public class CreateLocation {
    public static int getSizeX() {
        return sizeX;
    }

    public static void setSizeX(int sizeX) {
        CreateLocation.sizeX = sizeX;
    }

    private static int sizeX;

    public static int getSizeY() {
        return sizeY;
    }

    public static void setSizeY(int sizeY) {
        CreateLocation.sizeY = sizeY;
    }
    private static int sizeY;
    public String[][] CreateLocation(int x, int y){
        setSizeX(x+1);
        setSizeY(y+1);
        if((getSizeX()<27)&&(getSizeY()<27)&&(getSizeX()>0)&&(getSizeY()>0)) {
            String[][] array = new String[getSizeX()][getSizeY()];
            for (int i = 0; i < getSizeX(); i++) {
                if (i == 0) {
                    array[i][0] = " ";
                } else if(i<10){
                    array[i][0] = Integer.toString(i)+ " ";
                }else {
                    array[i][0] = Integer.toString(i);
                }
            }

            for (int j = 1; j < getSizeY(); j++) {
                if(j==1) {
                    array[0][j] = " " + java.lang.Character.toString((char) (96 + j));
                } else {
                    array[0][j] = java.lang.Character.toString((char) (96 + j));
                }
            }

            for(int l=1;l<getSizeX();l++) {
                for(int u=1;u<getSizeY();u++){
                    array[l][u] = "▢";
                }
            }
            return array;
        } else {
            throw new WrongArtifactTypeException("Wrong location size. Print it again");
        }
    }
}
