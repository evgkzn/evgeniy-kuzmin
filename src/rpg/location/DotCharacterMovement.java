package rpg.location;

public class DotCharacterMovement {
    private static DotCharacter dc;
    private static String[][] array;
    private static Location loc;

    public DotCharacterMovement(DotCharacter dcin, Location loc) {
       this.dc = dcin;
       this.array = loc.getLocation();
       this.loc = loc;
    }

    public static void move(String input) {
        DisplayLocation dl = new DisplayLocation();
        switch (input.toUpperCase()) {
            case "W":
                if (dc.getX() == 1) {
                    System.out.println("No way");
                } else if (array[dc.getX()-1][dc.getY()].equals(DotBot.getSymb())) {
                    array[dc.getX()][dc.getY()] = "▢";
                    dc.setX(dc.getX() - 1);
                } else if (dc.getX() != array.length) {
                    if(!array[dc.getX()][dc.getY()].equals(DotBot.getSymb())) {
                        array[dc.getX()][dc.getY()] = "▢";
                    }
                    dc.setX(dc.getX() - 1);
                    array[dc.getX()][dc.getY()] = dc.getSymb();
                } else {
                    System.out.println("No way");
                }
                break;
            case "A":
                if (dc.getY() == 1) {
                    System.out.println("No way");
                } else if (array[dc.getX()][dc.getY()-1].equals(DotBot.getSymb())) {
                    array[dc.getX()][dc.getY()] = "▢";
                    dc.setY(dc.getY() - 1);
                } else if (dc.getY() != array.length) {
                    if(!array[dc.getX()][dc.getY()].equals(DotBot.getSymb())) {
                        array[dc.getX()][dc.getY()] = "▢";
                    }
                    dc.setY(dc.getY() - 1);
                    array[dc.getX()][dc.getY()] = dc.getSymb();
                } else {
                    System.out.println("No way");
                }
                break;
            case "S":
                if (dc.getX()==array.length-1){
                    System.out.println("No way");
                } else if (array[dc.getX()+1][dc.getY()].equals(DotBot.getSymb())) {
                    array[dc.getX()][dc.getY()] = "▢";
                    dc.setX(dc.getX() + 1);
                } else if (dc.getX() != array.length - 1) {
                    if(!array[dc.getX()][dc.getY()].equals(DotBot.getSymb())) {
                        array[dc.getX()][dc.getY()] = "▢";
                    }
                    dc.setX(dc.getX() + 1);
                    array[dc.getX()][dc.getY()] = dc.getSymb();
                } else {
                    System.out.println("No way");
                }
                break;
            case "D":
                if(dc.getY()==array.length-1){
                    System.out.println("No way");
                } else if (array[dc.getX()][dc.getY()+1].equals(DotBot.getSymb())) {
                array[dc.getX()][dc.getY()] = "▢";
                dc.setY(dc.getY() + 1);
                } else if (dc.getY()!=array.length-1){
                    if(!array[dc.getX()][dc.getY()].equals(DotBot.getSymb())) {
                        array[dc.getX()][dc.getY()] = "▢";
                    }
                    dc.setY(dc.getY()+1);
                    array[dc.getX()][dc.getY()] = dc.getSymb();
                } else {
                    System.out.println("No way");
                }
                break;
                default: System.out.println("Wrong key");
                break;
        }
        dl.DisplayLocation(loc);
    }
}
