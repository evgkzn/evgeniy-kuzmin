package rpg.location;

public class Location {
    public Location(String[][] loc) {
        this.location = loc;
        this.x = loc.length;
        this.y = loc.length;
    }

    public String[][] getLocation() {
        return location;
    }

    private String[][] location;

    public int getX() {
        return x;
    }

    private int x;

    public int getY() {
        return y;
    }

    private int y;

    public int getChStartX() {
        return chStartX;
    }

    private int chStartX = 1;

    public int getChStartY() {
        return chStartY;
    }

    private int chStartY = 1;

    public String getSymb() {
        return symb;
    }

    private String symb = "▢";

    public void CheckForBattle(DotCharacter dc, DotBot db) {
        if ((dc.getX() == db.getX()) && (dc.getY() == db.getY())) {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("You are on the same cell with the enemy. Fights can not be avoided");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
    }
}
