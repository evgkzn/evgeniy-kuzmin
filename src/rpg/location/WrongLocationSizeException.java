package rpg.location;

public class WrongLocationSizeException extends RuntimeException{
    public WrongLocationSizeException(String ms) {
        super(ms);
    }
}
