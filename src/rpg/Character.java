package rpg;

public class Character {
    Artifact[] artifacts = new Artifact[5];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getOriginalHP() {
        return originalHP;
    }

    public void setOriginalHP(int originalHP) {
        this.originalHP = originalHP;
    }

    public int getOriginalStrength() {
        return originalStrength;
    }

    public void setOriginalStrength(int originalStrength) {
        this.originalStrength = originalStrength;
    }

    public int getOriginalArmor() {
        return originalArmor;
    }

    public void setOriginalArmor(int originalArmor) {
        this.originalArmor = originalArmor;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getLastHP() {
        return lastHP;
    }

    public void setLastHP(int lastHP) {
        this.lastHP = lastHP;
    }

    public int getLastArmor() {
        return lastArmor;
    }

    public void setLastArmor(int lastArmor) {
        this.lastArmor = lastArmor;
    }

    public int getLastStrength() {
        return lastStrength;
    }

    public void setLastStrength(int lastStrength) {
        this.lastStrength = lastStrength;
    }

    private boolean isAlive = true; // Alive or Not
    private int originalArmor = 0; // Start armor
    private int originalStrength = 10; // Start strength
    private int originalHP  = 100; // Start HP
    private int lastHP = originalHP; // Live HP
    private int lastArmor = originalArmor; // // Live Armor
    private int lastStrength = originalStrength; // // Live Strength

    private int armor = 0;
    private int level = 1;
    private int strength = 10;
    private double maxHit;
    private double minHit;
    private int hp = 100;
    private String name;

    public double getMinHit() {
        minHit = strength*0.5+level;
        return minHit;
    }

    public double getMaxHit(){
        maxHit = strength*1+level;
        return maxHit;
    }

    public void info() {
        System.out.println("Character's name: " + name);
        System.out.println("----------------------------");
        System.out.println("Level: " + level);
        System.out.println("HP: " + hp);                       // Get info about character
        System.out.println("Strength: " + strength);
        System.out.println("Armor: " + armor);
        System.out.println("----------------------------");
    }


    public void checkArtifacts() {
        System.out.println();
        System.out.println(name + "'s artifacts: ");
        for (Artifact artifact : artifacts) {              // Show all artifacts
            if(artifact!=null) {
                System.out.println(artifact.getType());
            }
        }
    }



    public void addArtifact(Artifact artifact) {
        switch (artifact.getType()){
            case "Weapon":
                if (artifacts[0]!=null) {
                    denyStats(artifacts[0]);
                }
                artifacts[0] = artifact;
                break;
            case "Helmet":
                if (artifacts[1]!=null) {
                    denyStats(artifacts[1]);
                }
                artifacts[1] = artifact;                     // Adding artifacts
                break;
            case "Chestplate":
                if (artifacts[2]!=null) {
                    denyStats(artifacts[2]);
                }
                artifacts[2] = artifact;
                break;
            case "Boots":
                if (artifacts[3]!=null) {
                    denyStats(artifacts[3]);
                }
                artifacts[3] = artifact;
                break;
            case "Shield":
                if (artifacts[4]!=null) {
                    denyStats(artifacts[4]);
                }
                artifacts[4] = artifact;
                break;

        }
        recalcStats();
    }

    private void recalcStats() {
        hp = lastHP;
        strength = lastStrength;
        armor = lastArmor;
        for (Artifact art : artifacts) {
            if (art!=null) {
                if (art.isNew() == true) {
                    hp = hp + art.getHp();                       // Recalculating stats
                    lastHP = hp;
                    armor = armor + art.getArmor();
                    lastArmor = armor;
                    strength = strength + art.getStrength();
                    lastStrength = strength;
                    art.setNew(false);
                }
            }
        }
    }

    private void denyStats(Artifact art) {
        lastHP = lastHP - art.getHp();
        lastStrength = lastStrength - art.getStrength();         // Deleting stats from deleted artifacts
        lastArmor = lastArmor - art.getArmor();
    }
}