package rpg;

public class Artifact {
    private String artTypes[] = {"Weapon", "Helmet", "Chestplate", "Boots", "Shield"};

    private String type;
    public Artifact(String type, int hp, int strength, int armor) {
        boolean found = false;
        for (String type1 : artTypes) {
            if (type.equals(type1)) {
                found = true;
            }
        }
        if (!found) {
            throw new WrongArtifactTypeException("Wrong artifact type: " + type);
        }
        this.type = type;
        this.hp = hp;
        this.strength = strength;
        this.armor = armor;
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getStrength() {
        return strength;
    }

    public int getArmor() {
        return armor;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    private boolean isNew = true;
    private int hp = 0;
    private int strength = 0;
    private int armor = 0;



}
