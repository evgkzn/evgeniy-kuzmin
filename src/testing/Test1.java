package testing;

import java.io.*;
import java.util.ArrayDeque;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.function.IntBinaryOperator;

public class Test1 {
    public static void main(String[] args) throws IOException {
        String str = "11";
        int i = Integer.decode(str);
        Integer i1 = new Integer(str);
        System.out.println(i1);
        Double doub = 1.01;
        System.out.println(doub.hashCode());
        String boolString = "fsrue";
        Boolean boolStr = Boolean.valueOf(boolString);
        System.out.println(boolStr);
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        IntBinaryOperator simpleAdd = (a, b) -> a + b;
        simpleAdd.applyAsInt(3,4);
        InputStream is = new FileInputStream("test.txt");
        DataInputStream dataInputStream = new DataInputStream(is);
        RandomAccessFile raf = new RandomAccessFile("words.txt", "r");
        String string = "";
        int b = raf.read();
        while (b!=-1){
            string+=(char)b;
            b = raf.read();
        }
        System.out.println(string);
        PrintWriter pw = new PrintWriter("words.txt");
        pw.write("lolkek");
        pw.close();
        FileWriter fw = new FileWriter("words.txt", true);
        fw.write("hello");
        fw.close();
        FileReader fl = new FileReader("words.txt");
        Scanner sc = new Scanner(fl);
        System.out.println(sc.nextLine());
        fl.close();
    }
}
