package task3;

import java.util.Scanner;

public class N3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Print count of strings: ");
        int count = sc.nextInt();

        for (int i=0; i<count;i++) {
            for (int j=1;j<=count-i;j++){
                System.out.print(" ");
            }
            for (int m=1;m<=2*i+1;m++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
