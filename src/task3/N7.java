package task3;
import java.util.Scanner;

public class N7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Print room length: ");
        double roomLength = sc.nextInt();
        System.out.println("Print room width: ");
        double roomWidth = sc.nextInt();
        System.out.println("Print table length: ");
        double tableLength = sc.nextDouble();
        System.out.println("Print table width: ");
        double tableWidth = sc.nextDouble();
        double distance = tableLength/2;
        int countLength = 0;
        int countWidth = 0;
        System.out.println("Distance = " + distance);

        while ((roomLength>=distance) && (roomWidth>=distance)) {
            if (roomLength> distance + tableLength) {
                roomLength = roomLength - distance - tableLength;
                if (roomLength>=distance) {
                    countLength++;
                }
                else {
                    roomLength = 0;
                }
            }
            else if (roomLength==distance) {
                roomLength=0;
                System.out.println();
            }
            else {
                roomLength = 0;
            }

            if (roomWidth>distance+tableWidth) {
                roomWidth = roomWidth - distance - tableWidth;
                if (roomWidth>=distance) {
                    countWidth++;
                }
                else {
                    roomWidth = 0;
                }
            }
            else if (roomWidth==distance) {
                roomWidth=0;
            }
            else {
                roomWidth = 0;
            }
        }
        System.out.println("You can put " + countLength*countWidth + " table(s) in your room.");
    }
}
