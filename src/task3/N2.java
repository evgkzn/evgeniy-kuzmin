package task3;

import java.util.Scanner;

public class N2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Print count of *: ");
        int count = sc.nextInt();

        for (int i=1;i<=count;i++) {
            for (int j=1;j<=count-i;j++){
                System.out.print(" ");
            }
            for (int m=1;m<=i;m++) {
                System.out.print("*");
            }
            System.out.println();
        }
        }
    }
