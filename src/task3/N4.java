package task3;
import java.util.Scanner;

public class N4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Print numbers to: ");
        int max = sc.nextInt();
        System.out.println("Number per string: ");
        int num = sc.nextInt();

        int n = 1;
        int k;
        if (max%num==0) {
            k = max/num;
        }
        else {
            k=max/num+1;
        }

        for (int i=1;i<=k;i++) {
            for (int j=1;j<=num;j++) {
                if (n<=max) {
                    if (n < 10) {
                        System.out.print("0" + n + " ");
                        n++;
                    } else {
                        System.out.print(n + " ");
                        n++;
                    }
                }
            }
            System.out.println();
        }
    }
}
