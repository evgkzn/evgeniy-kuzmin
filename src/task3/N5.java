package task3;
import java.util.Scanner;

public class N5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("You need to perform 1 to 21. Your number now is 1.");
        int num = 1;
        int docount = 0;
        System.out.println("+ and - make +1 or -1, if * or / you need to print on what");
        while (num!=21) {
            System.out.println("Choose your command: 1.+ | 2.- | 3./ | 4.* | Print only number");
            int dothis = sc.nextInt();
        switch(dothis) {
            case 1:
                num++;
                docount++;
                System.out.println("Your number is now: " + num);
                System.out.println();
                break;
            case 2:
                num--;
                docount++;
                System.out.println("Your number is now: " + num);
                System.out.println();
                break;
            case 3:
                System.out.println("Print divider: ");
                int di = sc.nextInt();
                num = num/di;
                docount++;
                System.out.println("Your number is now: " + num);
                System.out.println();
                break;
            case 4:
                System.out.println("Print multiplyer: ");
                int mp = sc.nextInt();
                num = num*mp;
                docount++;
                System.out.println("Your number is now: " + num);
                System.out.println();
                break;
            default:
                System.out.println("ERROR Wrong number");
                System.out.println();
                break;
        }

        }
        System.out.println("Game over!");
        System.out.println("Print your name: ");
        String name = sc.next();
        System.out.println("Congratulations, " + name + ", you won!");
        System.out.println("You did " + docount  + " steps");
    }
}
