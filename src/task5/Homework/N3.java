package task5.Homework;

public class N3 {
    public static void main(String[] args) {

        String str = "10 mm";

        double number;
        String numberString = str.substring(0, str.indexOf(' '));
        number = Double.parseDouble(numberString);
        System.out.print(str + " = " + number/10 + " cm = " + number/1000 + " m = " + number/25.4 + " in");
    }
}
