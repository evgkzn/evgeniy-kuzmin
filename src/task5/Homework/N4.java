package task5.Homework;

public class N4 {
    public static void main(String[] args) {

        String str = "Hello! My name is Evgeniy. How are you?";
        System.out.println("Original string: " + str);
        System.out.println();

        System.out.println("Replace e -> a : ");
        System.out.println(str.replace('e','a'));
        System.out.println();

        System.out.println("Replace {Evgeniy} -> {Eugene} : ");
        System.out.println(str.replaceAll("Evgeniy", "Eugene"));
        System.out.println();

        System.out.print("Find name in String : ");
        String name = str.substring(str.indexOf("is") + 3, str.indexOf('.') );
        System.out.println(name);
    }
}
