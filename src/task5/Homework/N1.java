package task5.Homework;

public class N1 {
    public static void main(String[] args) {

        String str = "Upper-lower case";

        for (int i = 0; i<str.length();i++) {
            if (i%2 != 0) {
                System.out.print(str.toUpperCase().charAt(i));
            } else {
                System.out.print(str.toLowerCase().charAt(i));
            }
        }
    }
}
