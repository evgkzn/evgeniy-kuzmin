package task12;

public class Boat implements Fuelable, Driveable{
    public int getFuel() {
        return fuel;
    }

    private int fuel = 0;
    public void setFuel(int i) {
        this.fuel=i;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    private int distance = 0;
}
