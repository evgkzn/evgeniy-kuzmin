package task12;

public class Demo {
    public static void main(String[] args) {
        Car car = new Car();
        Boat boat = new Boat();
        Plane plane = new Plane();
        Driver dr = new Driver();
        GasStation gs = new GasStation();
        gs.fill(car);
        dr.drive(car, 50);
        System.out.println("Car's - Fuel : " + car.getFuel()+ " Distance : " +car.getDistance());
        gs.fill(boat);
        dr.drive(boat, 30);
        System.out.println("Boat's - Fuel : " + boat.getFuel()+ " Distance : " +boat.getDistance());
        gs.fill(plane);
        dr.drive(plane,100);
        System.out.println("Plane's - Fuel : " + plane.getFuel()+ " Distance : " +plane.getDistance());


    }
}
