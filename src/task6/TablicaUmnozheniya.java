package task6;

import java.util.Arrays;

public class TablicaUmnozheniya {
    public static void main(String[] args) {
        int[][] array = new int[10][10];
        for (int i=0;i<10;i++) {
            for (int j=0;j<10;j++) {
                array[i][j] = (i+1)*(j+1);
            }
        }
        for(int m =0;m<10;m++) {
            System.out.println(Arrays.toString(array[m]));
    }

    }
}
