package task9.CheckedException;

public class HomeworkChecked {
    public static void main(String[] args) throws WrongSideSize {
            Triangle tr = new Triangle();
            try {
                tr.setSideA(1);
                tr.setSideB(2);
                tr.setSideC(-2);
            } catch (WrongSideSize e) {
                System.out.println("Error: " + e.getMessage());
                e.printStackTrace();
            }
    }
}
