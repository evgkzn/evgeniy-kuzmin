package task9.CheckedException;

public class WrongSideSize extends Exception{
    public WrongSideSize(String ms) {
        super(ms);
    }
}
