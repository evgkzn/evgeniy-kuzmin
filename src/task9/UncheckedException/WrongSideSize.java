package task9.UncheckedException;

public class WrongSideSize extends RuntimeException{
    public WrongSideSize(String ms) {
        super(ms);
    }
}
