package task9.UncheckedException;

import task9.CheckedException.WrongSideSize;

public class HomeworkUnchecked {
    public static void main(String[] args) throws WrongSideSize {
        Triangle tr = new Triangle();
        tr.setSideA(-1);
        tr.setSideB(4);
        tr.setSideC(5);
    }
}
