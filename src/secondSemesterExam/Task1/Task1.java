package secondSemesterExam.Task1;

import java.util.ArrayList;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(123);
        list.add(102);
        list.add(100);
        ModuleDiffComparator comparator = new ModuleDiffComparator();
        list.sort(comparator);
        System.out.println(list.toString());
    }
}
