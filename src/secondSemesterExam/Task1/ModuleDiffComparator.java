package secondSemesterExam.Task1;

import java.util.Comparator;

public class ModuleDiffComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        int o1Sum = 0;
        int o1Proiz = 1;
        int o2Sum = 0;
        int o2Proiz = 1;
        int o1Diff = 0;
        int o2Diff = 0;

        String o1S = o1.toString();
        String[] o1SA = o1S.split("");
        String o2S = o2.toString();
        String[] o2SA = o2S.split("");

        for (int i = 0; i<o1SA.length;i++){
            o1Sum += Integer.parseInt(o1SA[i]);
            o1Proiz *= Integer.parseInt(o1SA[i]);
        }

        for (int i = 0; i<o2SA.length;i++){
            o2Sum += Integer.parseInt(o2SA[i]);
            o2Proiz *= Integer.parseInt(o2SA[i]);
        }

        if (o1Sum-o1Proiz>0){
            o1Diff = o1Sum-o1Proiz;
        } else {
            o1Diff = o1Proiz-o1Sum;
        }

        if (o2Sum-o2Proiz>0){
            o2Diff = o2Sum-o2Proiz;
        } else {
            o2Diff = o2Proiz-o2Sum;
        }

        return o2Diff-o1Diff;
    }
}
