package secondSemesterExam.Task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task2 {
    public static int sum;
    public static int endId;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите массив. Все числа через пробел");
        String arrayS = sc.nextLine();
        String[] arraySA = arrayS.split(" ");
        int[] arrayInt = new int[arraySA.length];
        for (int i = 0; i<arraySA.length;i++){
            arrayInt[i] = Integer.parseInt(arraySA[i]);
        }
        endId = arrayInt.length-1;
        System.out.println("Введите k");
        int k = sc.nextInt();
        boolean isOk = false;
        while (!isOk) {
            if (k > arrayInt.length){
                System.out.println("Потоков больше, чем чисел - введите другое число потоков");
                k = sc.nextInt();
            } else {
                isOk = true;
            }
        }
        int howManyInt;
        if (arrayInt.length%k==0){
            howManyInt = arrayInt.length/k;
        } else {
            howManyInt = (arrayInt.length/k)+1;
        }

        int position = 0;
        int threadCounter = 0;
        Thread[] threads = new CounterThread[k];
        while (position<arrayInt.length){
            threads[threadCounter] = new CounterThread(position,position+howManyInt-1,arrayInt);
            position +=howManyInt;
            threadCounter++;
        }
        for (Thread thread : threads){
            thread.run();
        }
        System.out.println(sum);

    }
}
