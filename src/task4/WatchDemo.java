package task4;
import java.util.Scanner;

public class WatchDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Watch w = new Watch();
        System.out.println("Print capacity of your watch: ");
        w.b.setCapacity(sc.nextInt());
        switch (w.b.isError()) {
            case 0:
            for (int i = 0; i < 13; i++) {
                w.tick();
            }
            w.startCharging();
            for (int i = 0; i < 15; i++) {
                w.tick();
            }
            case 1:
                break;
        }
    }
}
