package task4.ClockAndAlarm;


public class Clock {
    int ticks = 101;
    Alarm al;
    public void tick() {
        for (int i=1;i<=ticks;i++) {
            if (al.getTime()>0) {
                al.setTime(al.getTime()-1);
                System.out.println("*TICKED* Timer expire in: " + al.getTime());
            } else if (al.getTime()==0) {
                System.out.println("ALARM! TIME IS OVER!");
            }
        }
    }
    public void addAlarm(Alarm al) {
        this.al = al;
        System.out.println("Alarm setted on " + al.getTime() + " ticks");
    }
}
