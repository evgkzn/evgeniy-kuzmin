package task4.ClockAndAlarm;

public class Alarm {
    public Alarm(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    private int time;
}
