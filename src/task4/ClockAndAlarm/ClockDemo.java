package task4.ClockAndAlarm;

public class ClockDemo {
    public static void main(String[] args) {
        Clock cl = new Clock();
        Alarm al = new Alarm(100);
        cl.addAlarm(al);
        cl.tick();
    }
}
