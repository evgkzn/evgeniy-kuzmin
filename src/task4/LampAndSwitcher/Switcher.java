package task4.LampAndSwitcher;

public class Switcher {
    private boolean switchedOn;

    Lamp lm = new Lamp();

    private void switchL() {
        if (switchedOn == true) {
            lm.setSwitchedOn(true);
        } else {
            lm.setSwitchedOn(false);
        }
    }


    public void switchOn() {
        switchedOn = true;
        switchL();
        System.out.println("You switched on the switcher. Switcher ON: " + switchedOn + " Lamp ON: " + lm.isSwitchedOn());
    }
    public void switchOff() {
        switchedOn = false;
        switchL();
        System.out.println("You switched off the switcher. Switcher ON: " + switchedOn + " Lamp ON: " + lm.isSwitchedOn());
    }
}
