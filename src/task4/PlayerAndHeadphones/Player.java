package task4.PlayerAndHeadphones;

public class Player {
    Headphones hd = new Headphones();
    private boolean playing;

    public void play() {
        if (playing == false) {
            playing = true;
            System.out.println("Music now playing!");
        } else {
            playing = false;
            System.out.println("Music now is NOT playing!");
        }
    }

    public void inject() {
        hd.setInjected(true);
        System.out.println("Headphones now injected!");
    }
    public void eject() {
        hd.setInjected(false);
        System.out.println("Headphones now ejected!");
    }
}
