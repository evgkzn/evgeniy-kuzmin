package task4.PlayerAndHeadphones;

public class Headphones {
    public boolean isInjected() {
        return injected;
    }

    public void setInjected(boolean injected) {
        this.injected = injected;
    }

    private boolean injected;
}
