package task4.PlayerAndHeadphones;

public class PlayerDemo {
    public static void main(String[] args) {
        Headphones hd = new Headphones();
        Player pl = new Player();

        pl.inject();
        pl.play();
        pl.play();
        pl.eject();
    }
}
