package task4;

public class Watch {
    Battery b = new Battery(10);
    boolean isOnCharging = false;
    public void tick() {
        if (isOnCharging == false) {
            b.decrease();
        }
        else {
            b.charge();
        }
        if (b.getCapacity() != 0) {
            System.out.println("*TICKED* Battery capacity now is: " + b.getCapacity());
        }
        else {
            System.out.println("Battery died. ");
        }
    }
    public void startCharging() {
        isOnCharging = true;
    }
    public void endCharging() {
        isOnCharging = false;
    }
}
