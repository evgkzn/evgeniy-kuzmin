package task4.TVAndOther;

public class TVAndOtherDemo {
    public static void main(String[] args) {
        PultTV pt = new PultTV();
        PultTVBox ptb = new PultTVBox();
        TV t = new TV();
        TVBox tb = new TVBox();

        pt.pressPowerButton();
        ptb.pressPowerButton();
        t.injectBox();
        for (int i=1;i<=2;i++) {
            pt.turnSoundDown();
        }
        for (int i=1;i<=2;i++) {
            pt.turnSoundUp();
        }
        ptb.switchTheChannel();
        pt.pressPowerButton();
        ptb.pressPowerButton();
        pt.turnSoundUp();
        ptb.switchTheChannel();
    }
}
