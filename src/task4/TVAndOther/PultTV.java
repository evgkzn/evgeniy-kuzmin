package task4.TVAndOther;

public class PultTV {
    Screen sc = new Screen();
    TV t = new TV();
    private int soundLevel = 10;
    public void turnSoundUp() {
        if ((soundLevel<50) && (t.isTurnedOn() == true)) {
            System.out.println("*PRESSED SOUND UP BUTTON ON TV PULT*");
            soundLevel++;
            System.out.println("Level of sound now is: " + soundLevel);
        } else if ((soundLevel == 50) && (t.isTurnedOn() == true)) {
            System.out.println("*PRESSED SOUND UP BUTTON ON TV PULT*");
            System.out.println("Level of sound is maxed out!");
        } else if (t.isTurnedOn() == false) {
            System.out.println("*PRESSED SOUND UP BUTTON ON TV PULT*");
            System.out.println("First of all turn ON TV");
        }
    }
    public void turnSoundDown() {
        if ((soundLevel>0) && (t.isTurnedOn() == true)) {
            System.out.println("*PRESSED SOUND DOWN BUTTON ON TV PULT*");
            soundLevel--;
            System.out.println("Level of sound now is: " + soundLevel);
        } else if ((soundLevel == 0) && (t.isTurnedOn() == true)){
            System.out.println("*PRESSED SOUND DOWN BUTTON ON TV PULT*");
            System.out.println("Sound muted!");
        }
        else if (t.isTurnedOn() == false) {
            System.out.println("*PRESSED SOUND DOWN BUTTON ON TV PULT*");
            System.out.println("First of all turn on TV");
        }
    }
    public void pressPowerButton() {
        if (t.isTurnedOn() == false) {
            System.out.println("*PRESSED POWER BUTTON ON TV PULT*");
            t.setTurnedOn(true);
            System.out.println("TV now turned ON ");
            sc.screenLitUp();
        } else if (t.isTurnedOn() == true) {
            System.out.println("*PRESSED POWER BUTTON ON TV PULT*");
            t.setTurnedOn(false);
            System.out.println("TV now turned OFF ");
            sc.screenTurnedOff();
        }

    }

}
