package task4.TVAndOther;

public class TV {
    TVBox tb = new TVBox();
    public boolean isTurnedOn() {
        return turnedOn;
    }

    public void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }

    private boolean turnedOn;
    public void injectBox() {
        tb.setInjected(true);

        System.out.println("TVBox injected ");
    }
    public void ejectBox() {
        tb.setInjected(false);
        System.out.println("TVBox ejected");
    }
}
