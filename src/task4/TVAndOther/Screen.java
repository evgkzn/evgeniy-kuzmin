package task4.TVAndOther;

public class Screen {
    private boolean screenOn;
    public void screenLitUp() {
        screenOn = true;
        System.out.println("Screen started working");
    }
    public void screenTurnedOff() {
        screenOn = false;
        System.out.println("Screen turned off");
    }
}
