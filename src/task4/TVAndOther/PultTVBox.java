package task4.TVAndOther;

public class PultTVBox {
    TVBox tb = new TVBox();
    public void switchTheChannel() {
        if (tb.isTurnedOn() == true) {
            System.out.println("*PRESSED SWITCHING CHANNEL BUTTON ON TV BOX PULT*");
            System.out.println("Channel switched");
        } else {
            System.out.println("*PRESSED SWITCHING CHANNEL BUTTON ON TV BOX PULT*");
            System.out.println("First of all turn on TVBox");
        }
    }

    public void pressPowerButton() {
        if (tb.isTurnedOn() == false) {
            System.out.println("*PRESSED POWER BUTTON ON TV BOX PULT*");
            tb.setTurnedOn(true);
            System.out.println("TV Box now turned ON ");
        } else if (tb.isTurnedOn() == true) {
            System.out.println("*PRESSED POWER BUTTON ON TV BOX PULT*");
            tb.setTurnedOn(false);
            System.out.println("TV Box now turned OFF " );
        }

    }
}
