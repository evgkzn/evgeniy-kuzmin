package task4;

public class Battery {
    public int isError() {
        return error;
    }

    private int error = 0;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        if ((capacity <=10) && (capacity >=0)) {
            this.capacity = capacity;
        }
        else {
            System.out.println("ERROR! WRONG CAPACITY!");
            error = 1;
        }
    }

    public Battery(int capacity) {
        this.capacity = capacity;
    }

    private int capacity = 0;

    public void decrease() {
        this.capacity--;
        if (this.capacity<0) {
            this.capacity = 0;
        };
    }

    public void charge() {
        this.capacity++;
        if (this.capacity >10) {
            this.capacity = 10;
        };
    }


}
