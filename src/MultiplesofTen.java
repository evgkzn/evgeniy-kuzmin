public class MultiplesofTen {
    public static void main(String[] args) {
        int kol = 0;
        for (int i=1; i<=100; i++) {
            if (i%10 == 0) {
                System.out.println(i);
                kol++;
            }
        }
        System.out.println();
        System.out.println("Count of I%10 = 0: " + kol);

    }
}
