package task7;

public class Calc {
    public static void main(String[] args) {
        if (args.length>2) {
            switch (args[1]) {
                case "+":
                    System.out.println("Sum: " + sumArgs(Integer.parseInt(args[0]), Integer.parseInt(args[2])));
                    break;
                case "-":
                    System.out.println("Dif: " + difArgs(Integer.parseInt(args[0]), Integer.parseInt(args[2])));
                    break;
                case "*":
                    System.out.println("Mult: " + multArgs(Integer.parseInt(args[0]), Integer.parseInt(args[2])));
                    break;
                case "/":
                    System.out.println("Div: " + divArgs(Integer.parseInt(args[0]), Integer.parseInt(args[2])));
                    break;
            }
        } else System.out.println("ERROR! Check your expression.");
    }

    public static int sumArgs(int a, int b){
        return a+b;
    }
    public static int difArgs(int a,int b){
        return a-b;
    }
    public static int multArgs(int a,int b){
        return a*b;
    }
    public static int divArgs(int a,int b){
        return a/b;
    }
}
