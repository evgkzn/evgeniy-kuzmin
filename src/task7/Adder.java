package task7;

public class Adder {
    static int sumArgs;
    public static void main(String[] args) {
        if (args.length > 1) {
            for (int i = 0; i < args.length; i++) {
                sumArg(Integer.parseInt(args[i]));
            }
            System.out.println("Sum of args: " + getSumArgs());
        } else {
            System.out.println("ERROR. Print at least 2 arguments.");
        }
    }

    public static void sumArg(int arg){
        sumArgs = sumArgs+arg;
    }
    public static int getSumArgs() {
        return sumArgs;
    }
}
