package task7;

public class Anagram {
    public static void main(String[] args) {
        System.out.println("Answer is: " + conv(args[0], args[1]));
    }

    static String word1;
    static String word2;
    public static boolean conv(String a, String b) {
        Boolean isTrue = null;
        Character word1[] = new Character[a.length()];
        Character word2[] = new Character[b.length()];
        if (a.length()!=b.length()) {
            isTrue = false;
        } else {
            for (int j=0;j<a.length();j++) {
                for (int k=0;k<b.length();k++) {
                    if (word1[j]==word2[k]) {
                        word1[j] = '0';
                        word2[k] = '0';
                    }
                }
            }

            for (int j=0;j<a.length();j++) {
                for (int k=0;k<b.length();k++) {
                    if ((word1[j] != '0') || (word2[k]) != '0') {
                        isTrue=false;
                    } else {
                        isTrue=true;
                    }
                        
                    }
                }
        }
        return isTrue;
    }
}

