package task7;

public class N1 {
    public static void main(String[] args) {
        System.out.println("Byte: max: " + Byte.MAX_VALUE + " min: " + Byte.MIN_VALUE);
        System.out.println("Double: max: " + Double.MAX_VALUE + " min: " + Double.MIN_VALUE);
        System.out.println("Float: max: " + Float.MAX_VALUE + " min: " + Float.MIN_VALUE);
        System.out.println("Integer: max: " + Integer.MAX_VALUE + " min: " + Integer.MIN_VALUE);
        System.out.println("Long: max: " + Long.MAX_VALUE + " min: " + Long.MIN_VALUE);
        System.out.println("Short: max: " + Short.MAX_VALUE + " min: " + Short.MIN_VALUE);
    }
}
