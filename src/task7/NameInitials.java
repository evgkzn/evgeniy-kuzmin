package task7;

public class NameInitials {
    public static void main(String[] args) {
        for (int i=0;i<args.length;i++) {
            makeInit(args[i]);
        }
        System.out.println("Your initials: " + getInitials());
    }
    static String initials = "";
    public static void makeInit(String arg) {
        initials = initials + arg.substring(0,1) + ". ";
    }
    public static String getInitials(){
        return initials;
    }
}
